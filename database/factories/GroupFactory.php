<?php

use Faker\Generator as Faker;

$factory->define(App\Group::class, function (Faker $faker) {
		$exampleSystems = ['Call of Cthulhu 6th Edition', 'Call of Cthulhu 7th Edition', 'D&D 3.5E', 'D&D 5E', 'Shadowrun', 'Warhammer Fantasy 2nd Edition', 'Deathwatch', 'Pathfinder'];

    return [
      'name' => 'Example Group',
    ];
});
