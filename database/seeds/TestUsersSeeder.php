<?php

use App\Character;
use App\Group;
use App\GroupGameSystem;
use App\User;
use Illuminate\Database\Seeder;

class TestUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this -> deleteDatabaseEntries();
      $this -> call(GameSystemSeeder::class);


      $this -> createAdminUsers();
      $this -> createGroupsForAdminUsers();

      $this -> letPrimaryUserJoinMoreGroupsAsPlayer();
      $this -> createCharactersForSomeUsers();
    }

    private function deleteDatabaseEntries()
    {
      DB::table('group_game_systems')->delete();
      DB::table('game_systems')->delete();
      DB::table('group_user')->delete();
      DB::table('characters')->delete();
      DB::table('groups')->delete();
      DB::table('users')->delete();
    }

    private function createAdminUsers()
    {
        factory(User::class)->create([
            'email' => 'm.strathausen@gmail.com',
            'name' => 'strattegic'
        ]);

        factory(User::class)->create([
            'email' => 'm.strathausen@googlemail.com',
            'name' => 'Stratti2'
        ]);

        factory(User::class, 10)->create();
    }

    private function createGroupsForAdminUsers()
    {
      $users = User::get();

      foreach ($users as $user) 
      {
        $groups = factory(Group::class, 3)->make()
          ->each(function($group){
            $group -> name = 'Example Group #'. uniqid();
            $group -> save();

            $gameSystem = factory(GroupGameSystem::class)->create([
              'group_id' => $group -> id
            ]);
          });

        // attach users to groups
        $groups -> each(function($group) use ($user){
            $group -> users() -> save( $user, ['is_admin' => true] );
        });
      }
    }

    private function letPrimaryUserJoinMoreGroupsAsPlayer()
    {
      $groups = Group::get();
      $users = User::get();

      $groups -> each(function($group) use ($users){

        $usersNotInGroup = $users -> whereNotIn('id', $group->users->pluck('id')->all());

        $group -> users() -> attach($usersNotInGroup -> random(rand(1,6)));
      });
    }

    private function createCharactersForSomeUsers()
    {
      $users = User::has('groups')->get();

      $users -> each( function($user)
      {

        $user -> groups -> each( function ($group) use ($user)
        {
          if( rand(1,6) != 6 )
          {
            $charCount = rand(1,3);

            for ($i=1; $i <= $charCount; $i++) 
            {
              $active = $i != $charCount;

              // Set the last character as "active"
              factory(Character::class) 
              -> create([
                'user_id' => $user -> id,
                'group_id' => $group -> id,
              ]);
            }
          }
        });

      });
    }
}
