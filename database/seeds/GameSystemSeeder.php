<?php

use Illuminate\Database\Seeder;
use App\GameSystem;

class GameSystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Default game system
      GameSystem::create(['id' => config('tablegamers.game_system_default_id'), 'name' => 'app.game_system.default.title']);
    }
}
