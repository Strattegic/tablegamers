<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupGameSystemsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('group_game_systems', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('group_id')->unsigned();
      $table->foreign('group_id')->references('id')->on('groups');


      $table->integer('game_system_id')->nullable();
      $table->foreign('game_system_id')->references('id')->on('game_systems');

      $table->boolean('is_custom_system')->default(false);
      $table->string('custom_name', 255) -> nullable();
      $table->integer('custom_file_id')->unsigned() -> nullable();
      $table->foreign('custom_file_id')->references('id')->on('files');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('group_game_systems');
  }
}
