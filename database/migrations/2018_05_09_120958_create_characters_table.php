<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharactersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('characters', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users');
      $table->integer('group_id')->unsigned()->nullable();
      $table->foreign('group_id')->references('id')->on('groups');

      // only 1 character per user per group
      $table->index(['user_id', 'group_id']);

      $table->string('name', 255)->nullable();
      $table->text('description') -> nullable();
      $table->string('image')->nullable();
      $table->text('additional_data')->nullable();

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('characters');
  }
}
