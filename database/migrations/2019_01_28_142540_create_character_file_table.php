<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterFileTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('character_file', function (Blueprint $table) {
      $table->increments('id');

      $table->string('file_type', 255);

      $table->integer('character_id')->unsigned() -> nullable();
      $table->foreign('character_id')->references('id')->on('characters');

      $table->integer('file_id')->unsigned() -> nullable();
      $table->foreign('file_id')->references('id')->on('files');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('character_file');
  }
}
