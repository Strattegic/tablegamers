<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupInvitationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('group_invitations', function (Blueprint $table) {

      $table->increments('id');

      $table->integer('group_id')->unsigned();
      $table->foreign('group_id')->references('id')->on('groups');

      $table->integer('user_id')->unsigned()->nullable();
      $table->foreign('user_id')->references('id')->on('users');

      $table->integer('inviter_user_id')->unsigned()->nullable();
      $table->foreign('inviter_user_id')->references('id')->on('users');

      $table->string('name', 255);
      $table->string('email', 255);

      $table->softDeletes();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('group_invitations');
  }
}
