<?php

namespace App\Http\View\Composers;

use App\Group;
use App\Character;
use Illuminate\View\View;

class GroupLayoutComposer
{

  /**
   * Bind data to the view.
   *
   * @param  View  $view
   * @return void
   */
  public function compose(View $view)
  {
    $activeCharacterCount = Character::where('group_id', $view['group'] -> id)
    -> count();

    // is the current user a member of this group?
    $userIsInCurrentGroup = !empty( $view['group'] -> users -> firstWhere('id', auth()->id()) );

    $pendingInvitation = null;
    if( !empty( auth()->user()->invitations ) )
    {
      $pendingInvitation = auth()->user()->invitations->firstWhere('group_id', $view['group'] -> id);
    }

    $alerts = $this -> getAlerts($view['group']);

    $view->with(compact('activeCharacterCount', 'userIsInCurrentGroup', 'pendingInvitation', 'alerts'));
  }

  private function getAlerts( $group )
  {
    $alerts = [];
    $hasCharactersInGroup = Character::where('user_id', auth()->id())
    -> where('group_id', $group->id) -> count() > 0;

    if( !$hasCharactersInGroup )
    {
      $alerts[] = __('group.before_ready_alerts.no_character', ['url' => route('group.character.create', [$group])]);
    }
    return $alerts;
  }
}