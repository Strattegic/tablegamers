<?php

namespace App\Http\View\Composers;

use App\Group;
use App\Character;
use Illuminate\View\View;

class CompanionLayoutComposer
{

  /**
   * Bind data to the view.
   *
   * @param  View  $view
   * @return void
   */
  public function compose(View $view)
  {
    $groups = auth( )-> user() -> groups -> sortByDesc('updated_at');

    $mainNavGroupsAsAdmin = collect();
    $mainNavGroupsAsPlayer = collect();

    foreach( $groups as $group )
    {
      if( $group -> pivot -> is_admin && $mainNavGroupsAsAdmin -> count() < 3 )
      {
        $mainNavGroupsAsAdmin -> push($group);
      }
      else if( $mainNavGroupsAsPlayer -> count() < 3 )
      {
        $mainNavGroupsAsPlayer -> push($group);
      }
    }
    
    $view -> with(compact(['mainNavGroupsAsAdmin', 'mainNavGroupsAsPlayer']));
  }

}