<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if( Auth::check() )
      {
        $groups = Auth::user() -> groups -> sortByDesc('updated_at');

        $groupsAsAdmin = collect();
        $groupsAsPlayer = collect();

        foreach( $groups as $group )
        {
          if( $group -> pivot -> is_admin )
          {
            $groupsAsAdmin -> push($group);
          }
          else
          {
            $groupsAsPlayer -> push($group);
          }
        }

        return view('companion.home') -> with(compact(['groupsAsAdmin', 'groupsAsPlayer', 'groups']));
      }
      return view('public.home');
    }

    public function showFeatures()
    {
        return "features";
    }
}
