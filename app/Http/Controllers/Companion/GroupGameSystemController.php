<?php

namespace App\Http\Controllers\Companion;

use App\File;
use App\Group;
use App\GroupGameSystem;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GroupGameSystemController extends Controller
{
  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Group $group, GroupGameSystem $game_system)
  {
    $this -> authorize('update', [$game_system, $group]);

    $request->validate([
      'custom_name' => 'required|max:255',
      'game_system_id' => 'required|exists:game_systems,id',
    ]);

    if( $request -> game_system_id == config('tablegamers.game_system_default_id') )
    {
      $game_system -> custom_name = $request -> custom_name;
      $game_system -> game_system_id = $request -> game_system_id;

      if( $request -> has('custom_file') )
      {
        $path = $request -> custom_file -> store(config('tablegamers.group_character_sheet_folder'), 'public');

        $file = new File();
        $file -> uploader_user_id = auth()->id();
        $file -> mime_type = $request -> custom_file -> getMimeType();
        $file -> name = config('tablegamers.game_system_default_file_name');
        $file -> path = $path;
        $file -> save();

        $game_system -> custom_file_id = $file -> id;
      }

      $game_system -> save();
    }

    return back();
  }
}
