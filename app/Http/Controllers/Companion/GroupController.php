<?php

namespace App\Http\Controllers\Companion;

use App\Character;
use App\GameSystem;
use App\Group;
use App\GroupGameSystem;
use App\GroupInvitation;
use App\Http\Controllers\Controller;
use App\Models\Misc\GroupProgress;
use App\Notifications\GroupInvitationMail;
use App\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Validator;

class GroupController extends Controller
{

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $gameSystems = GameSystem::get();
    return view('companion.group.group_create') -> with(compact('gameSystems'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $request->validate([
      'name' => 'required|max:255'
    ]);

    $group = new Group;
    $group -> name = $request -> name;
    $group -> save();

    GroupGameSystem::create(['group_id' => $group -> id, 'is_custom_system' => true, 'game_system_id' => config('tablegamers.game_system_default_id')]);

    // intermediate table
    Auth::user()->groups()->attach($group, ['is_admin' => true]);

    return redirect() -> route('home');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Group $group, $playerIdentifier = null)
  {
    $this -> authorize('view', $group);
    // if( Gate::denies('view', $group) )
    // {
    //   // Show the "this group is private message"
    //   return view('companion.group.group_private') -> with(compact(['group']));
    // }

    $group -> load(['users', 'characters', 'system']);
    $isGroupAdmin = Auth::user()->isGroupAdmin( $group -> id );

    // calculate group progress
    $progress = new GroupProgress();
    $progress -> calculate($group, auth()->user(), auth()->user()->can('edit', $group));

    return view('companion.group.group')->with(compact(['group', 'isGroupAdmin', 'progress']));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Group $group)
  {
    $this -> authorize('edit', $group);
    $gameSystems = GameSystem::get();

    return view('companion.group.group_edit')->with(compact('group', 'gameSystems'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Group $group)
  {
    $this -> authorize('edit', $group);
    $request->validate([
      'name' => 'required|max:255',
      'image' => 'image|max:5000',
      // 'next_session_datetime' => 'date',
      'next_session_location' => 'max:255',
      'is_private' => 'boolean',
    ]);

    // find the group and update the values
    $group -> name = $request -> name;
    $group -> description = $request -> description;

    $group -> next_session_datetime = $request -> next_session_datetime;
    $group -> next_session_location = $request -> next_session_location;

    // TODO: If public groups are supported, this needs to be adjusted!
    $group -> is_private = true;

    // store the image
    if( $request -> has('image') ){
      $path = $request -> image -> store(config('tablegamers.group_images_folder'), 'public');
      $group -> image = $path;
    }

    $group -> save();

    return redirect()->route('group.show', [$group->id]);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Group $group)
  {
    $this -> authorize('edit', $group);

    $group -> delete();

    return redirect() -> route('home');
  }

  public function showCharacters(Request $request, Group $group)
  {
    $this -> authorize('view', $group);

    $usersWithoutCharacter = $group -> users() -> whereDoesntHave('characters', function($query) use ($group){
      $query -> where('group_id', $group->id);
    }) -> get();

    // sort users so that the current authenticated user is first
    $usersWithoutCharacter = $usersWithoutCharacter -> sortBy(function ($user, $key) {
      if( $user -> id == auth()->id() )
        return -1;
      else
        return $user['name'];
      });

    $characters = $group -> characters;

    return view('companion.group.group_characters')->with(compact('group', 'characters', 'usersWithoutCharacter'));
  }

  public function inviteUser(Request $request, Group $group)
  {
    $this -> authorize('edit', $group);
    if( !Auth::user() -> isGroupAdmin( $group -> id ) )
    {
      return back();
    }

    // Validate the input
    $messages = [
      'email.unique' => 'A user with that :attribute was already invited.',
    ];

    $email = $request->input('email');

    $rules = [
      'name' => 'max:255',
      'email' => [
        'required', 
        'email', 
        'max:255', 
        Rule::unique('group_invitations')->where(function ($query) use ($group, $email) {
          return $query->where('group_id', $group->id)
          ->where('email', $email);
        })
      ]
    ];

    Validator::make($request->all(), $rules, $messages) -> validate();

    // Send the invitation if the validation was successful
    $invitation = GroupInvitation::create([
      'inviter_user_id' => Auth::user() -> id,
      'group_id' => $group -> id,
      'name' => $request -> name,
      'email' => $request -> email,
    ]);

    $this -> sendInvitationMail( $invitation, $group -> name );

    return back();
  }

  // Sends a notification to the invited player
  // contains all the information needed to join the group
  protected function sendInvitationMail( $invitation, $groupName )
  {
    $inviterName = Auth::user()->name;

    Notification::route('mail', $invitation -> email)
      ->notify(new GroupInvitationMail( $invitation, $inviterName, $groupName ));
  }
}