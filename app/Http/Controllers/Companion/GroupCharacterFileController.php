<?php

namespace App\Http\Controllers\Companion;

use App\Character;
use App\CharacterFile;
use App\Group;
use App\File;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class GroupCharacterFileController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Group $group, Character $character)
    {
      $request -> validate([
        'file' => 'required|file|max:5000',
        'name' => 'required|max:255',
        'file_type' => [
          'required',
          Rule::in(config('tablegamers.file_types')),
        ]
      ]);

      // store the file
      if( $request -> has('file') )
      {
        $path = $request -> file -> store('character_files', 'public');
        $file = new File();
        $file -> uploader_user_id = $character -> user -> id;
        $file -> name = $request -> name;
        $file -> mime_type = $request -> file -> getMimeType();
        $file -> path = $path;

        $character -> files() -> save($file, ['file_type' => $request -> file_type]);

        return back() -> with(['success' => 'File uploaded successful']);
      }
      return back() -> with(['error' => 'Nope!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Group $group, Character $character, CharacterFile $file)
    {
      $characterFile = Storage::disk('public')->path($file -> filename);
      return response()->file($characterFile);
    }

    /**
     *  Download a specific character file
     *
     */
    public function download(Request $request, Group $group, Character $character, CharacterFile $file)
    {
      return Storage::disk('public')->download($file -> filename);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Group $group, Character $character, CharacterFile $file)
    {
      $file -> delete();
      return back();
    }
}
