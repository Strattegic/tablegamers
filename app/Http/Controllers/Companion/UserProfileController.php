<?php

namespace App\Http\Controllers\Companion;

use Validator;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class UserProfileController extends Controller
{
  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(User $user)
  {
    if($user->can('update', $user))
    {
      return view('companion.user.user_edit')->with(compact(['user']));
    }
    return view('companion.user.user')->with(compact(['user']));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, User $user)
  {
    $this -> authorize('update', $user);

    $validator = Validator::make($request->all(), [
      'name' => 'required|max:255',
    ]);

    if( $validator -> fails() )
    {
      return back()
        ->withErrors($validator, 'settings')
        ->withInput();
    }

    $user -> name = $request -> name;
    $user -> save();
    return back() -> with(['success' => __('user.settings_changed_success')]);
  }

  public function changePassword(Request $request, User $user)
  {
    $this -> authorize('update', $user);

    $validator = Validator::make($request->all(), [
      'password' => 'max:255',
      'new_password' => 'required|string|confirmed|min:6',
    ]);

    if( $validator -> fails() )
    {
      return back()
        ->withErrors($validator, 'changePassword')
        ->withInput();
    }

    $user -> password = Hash::make($request -> password);
    $user -> save();
    return back() -> with(['success' => __('user.password_reset_success')]);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(User $user)
  {
    $this -> authorize('delete', $user);
  }
}
