<?php

namespace App\Http\Controllers\Companion;

use App\Group;
use App\User;
use App\Character;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupUserController extends Controller
{
    public function index(Request $request, Group $group)
    {
      // sort users so that the current authenticated user is first
      $users = $group -> users -> sortBy(function ($user, $key) {
        if( $user -> id == auth()->id() )
            return -1;
        else
            return $user['name'];
      });

      $isGroupAdmin = auth()->user()->isGroupAdmin( $group -> id );

      foreach( $users as $user )
      {
        $user -> is_group_admin = $user -> isGroupAdmin( $group -> id );
        $user -> is_deletable = $this -> userIsDeletable($group, $user);
      }

      return view('companion.group.group_users')->with(compact('users', 'group', 'isGroupAdmin'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Group $group, User $user)
    {
      $isGroupAdmin = auth()->user()->isGroupAdmin( $group -> id );

      foreach( $group -> users as $u )
      {
        if( $u->id == $user->id )
        {
          $user -> joined_group_at = $u -> pivot -> created_at;
          break;
        }
      }

      return view('companion.group.group_user')->with(compact('user', 'group', 'isGroupAdmin'));
    }

    public function destroy(Request $request, Group $group, User $user)
    {
      $this -> authorize('deteleUser', [$group, $user]);

      if( $this -> userIsDeletable($group, $user) )
      {
        $group -> users() -> detach( $user -> id );

        // delete all user content from this user in this group
        Character::where('user_id', $user -> id) -> where('group_id', $group -> id)->delete();
      }
      return back();
    }

    private function userIsDeletable($group, $user)
    {
      // check if the current user can delete someone
      if( $user -> id == auth()->id() || auth()->user()->isGroupAdmin( $group -> id ) )
      {
        if( $user -> isGroupAdmin( $group -> id ) )
        {
          // Group admins can't remove themself if there is no other admin
          $groupUsers = $group -> users;

          foreach( $groupUsers as $groupUser )
          {
            // check if there is another group admin
            if( $groupUser -> id != $user -> id && $groupUser -> pivot -> is_admin == true )
            {
              // there is another user with admin rights, so this user can be deleted
              return true;
            }
          }
          return false;
        }
        return true;
      }
    }
}
