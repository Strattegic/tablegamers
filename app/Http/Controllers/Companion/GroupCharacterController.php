<?php

namespace App\Http\Controllers\Companion;

use Illuminate\Http\Request;
use App\Group;
use App\User;
use App\Character;
use App\Http\Controllers\Controller;

class GroupCharacterController extends Controller
{

  public function show(Request $request, Group $group, User $user)
  {
    $character = Character::firstOrNew(['user_id' => $user->id, 'group_id' => $group->id]);

    $this -> authorize('view', $group);
    if( !empty( $character -> additional_data ) )
    {
      $character -> additional_data = json_decode($character -> additional_data, true);
    }

    $files = $character -> files;
    $characterSheet = null;

    $files = $files -> filter(function ($value, $key) use ($characterSheet) {
      if( $value -> file_type == config('tablegamers.file_types.character_sheet') )
      {
        $characterSheet = $value;
        return false;
      }
    });

    if( auth() -> id() == $user -> id )
    {
      return view('companion.group_character.character_edit') -> with(compact('group', 'character', 'files'));
    }
    return view('companion.group_character.character') -> with(compact('group', 'character', 'files', 'characterSheet'));
  }

  public function update(Request $request, Group $group)
  {
    $request -> validate([
      'name' => 'required|max:255',
      'image' => 'image|max:5000',
      'gender' => 'max:255',
      'age' => 'nullable|integer',
      'race' => 'max:255',
      'classname' => 'max:255',
    ]);

    $character = Character::firstOrCreate(['user_id' => auth()->user()->id, 'group_id' => $group -> id]);
    $character -> fill( $request -> all() );

    // collect additional data from request
    $requestData = $request -> all();
    unset($requestData['_token']);
    unset($requestData['_method']);
    $requestDataJson = json_encode($requestData);
    $character -> additional_data = $requestDataJson;

    if( $request -> has('image') ){
      $path = $request -> image -> store('character_images', 'public');
      $character -> image = $path;
    }

    $character -> save();
    return back();
  }
}
