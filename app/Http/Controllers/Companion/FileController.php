<?php

namespace App\Http\Controllers\Companion;

use App\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
  public function download(Request $request, File $file)
  {
    return Storage::disk('public')->download($file->path);
  }

  public function show(Request $request, File $file)
  {
    return response() -> file( Storage::disk('public')->path($file->path) );
  }

  public function destroy(Request $request, File $file)
  {
    $file -> delete();
    return back();
  }
}
