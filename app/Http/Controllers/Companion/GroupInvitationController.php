<?php

namespace App\Http\Controllers\Companion;

use App\Group;
use App\GroupInvitation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GroupInvitationController extends Controller
{
	public function accept( Group $group, GroupInvitation $groupInvitation )
	{
		$this -> checkAccess($group, $groupInvitation);

		// add the user to the group
		$group -> users() -> save( Auth::user() );

		// remove the invite
		$groupInvitation -> delete();

		return back();
	}

	public function reject( Group $group, GroupInvitation $groupInvitation )
	{
		$this -> checkAccess($group, $groupInvitation);

		// remove the invite
		$groupInvitation -> delete();

		return redirect() -> route('home');
	}

  /**
  * Force deletes a specific invitation.
  * Is used if you want uninvite a user while the invitation is still pending
  *
  */
  public function destroy( Group $group, GroupInvitation $groupInvitation )
  {
    $this -> checkAccess($group, $groupInvitation);

    // remove the invite
    $groupInvitation -> forceDelete();

    return redirect() -> back();
  }

	private function checkAccess( Group $group, GroupInvitation $invitation )
	{
		// check for the authenticated user
		if( Auth::user() -> id == $invitation -> user_id && $group -> id == $invitation -> group_id )
		{
			return;
		}

		if( $group -> users -> firstWhere('user_id', $invitation -> user_id ) )
		{
			// This user is already in the group!
			return back();
		}

		return back();
	}
}
