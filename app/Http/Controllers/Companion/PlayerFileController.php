<?php

namespace App\Http\Controllers\Companion;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\PlayerFile;

class PlayerFileController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($groupId, $playerId, Request $request)
    {
        // dd( $request->file('player_file') );

        $request->validate([
            'player_file' => 'required|file',
        ]);

        $uploadedFile = $request->file('player_file');

        $playerFile = new PlayerFile();
        $playerFile -> player_id = $playerId;
        $playerFile -> name = Input::file('player_file')->getClientOriginalName();

        $path = $uploadedFile->store('player_files', 'public');

        $playerFile -> filename = $path;
        $playerFile -> mime_type = $uploadedFile -> getMimeType();
        $playerFile -> save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
