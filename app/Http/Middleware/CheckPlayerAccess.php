<?php

namespace App\Http\Middleware;

use Closure;
use App\Player;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CheckPlayerAccess
{
    /**
     * This middleware checks if the current visitor is authorized to acccess this player
     * There are 2 ways he can gain access
     * 1. providing an identifier with the link (here called player_identifier)
     * 2. being an authorized user
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $id = null)
    {
        // already authorized users do not need to authorize again
        if( Auth::check() )
        {
            return $next($request);
        }

        // the visitor is not authenticated. Check for an identifier
        if( $request -> has(config('companion.player_identifier_key_short')) )
        {
            // determine if the player has the right to view this profile
            // 1. the given identifier must comply with the one in the database
            // 2. the player must be a player in the given group
            $groupId = $request -> route() -> parameter('group');
            $playerId = $request -> route() -> parameter('player');
            $playerIdentifier = $request -> get(config('companion.player_identifier_key_short'));

            // run a query that follows all the rules above
            $player = Player::where('group_id', $groupId) -> where('player_identifier', $playerIdentifier) -> find($playerId);

            // the player was returned, so the current visitor has access
            if( !empty( $player ) )
            {
                // if this is the first time that the player visits his view
                // set the invitation marker to already visited
                if( $player -> is_invitation_pending )
                {
                    $player -> is_invitation_pending = false;
                    $player -> save();
                }

                Session::flash('player_identifier', $playerIdentifier);
                return $next($request);
            }
        }

        // if the current visitor does not have an identifier, or entered wrong data
        // he needs to authorize!
        Auth::authenticate();

        // this is just a precaution
        // if something goes wrong with the Authentication middleware, we redirect
        //  the user to the home page, just to be safe
        return redirect() -> route('home');

    }
}
