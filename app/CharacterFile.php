<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CharacterFile extends Model
{
  protected $table = 'files';

  protected $fillable = ['character_id', 'user_id'];
}
