<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupGameSystem extends Model
{
  protected $fillable = ['group_id', 'is_custom_system', 'game_system_id'];
  protected $append = ['file'];

  public function group()
  {
    return $this -> hasOne('App\Group');
  }

  public function gameSystem()
  {
    return $this -> hasOne('App\GameSystem');
  }

  public function customFile()
  {
    return $this -> belongsTo('App\File', 'custom_file_id');
  }
}
