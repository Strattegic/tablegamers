<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameSystem extends Model
{
  protected $fillable = ['id', 'name'];
}
