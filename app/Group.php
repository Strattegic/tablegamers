<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Group extends Model
{
	protected $fillable = ['name', 'game_type', 'description'];
  protected $dates = ['next_session_datetime'];

	/**
	*	Retrieves the Group image. If there is none, it returns a placeholder image.
	*
	**/
	public function getImageAttribute($value)
  {
  	if( empty( $value ) )
  	{
  		return asset( config('tablegamers.group_placeholder_image') );
  	}

  	return Storage::url($value);
  }

  public function setNextSessionDatetimeAttribute($value)
  {
    $this -> attributes['next_session_datetime'] = \Carbon\Carbon::parse($value);
  }

  public function users()
  {
  	return $this -> belongsToMany('App\User')->withPivot('is_admin')->withTimestamps();
  }

  public function invitations()
  {
    return $this -> hasMany('App\GroupInvitation');
  }

  public function characters()
  {
    return $this -> hasMany('App\Character');
  }

  public function system()
  {
    return $this -> hasOne('App\GroupGameSystem');
  }
}
