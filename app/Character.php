<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
  protected $fillable = ['user_id', 'group_id', 'name', 'gender', 'age', 'race', 'classname', 'description', 'is_active'];

  public function user()
  {
    return $this -> belongsTo('App\User');
  }

  public function group()
  {
    return $this -> belongsTo('App\Group');
  }

  public function getImageAttribute($value)
  {
    if( empty( $value ) )
    {
      return asset( config('tablegamers.character_placeholder_image') );
    }

    return Storage::url($value);
  }

  public function characterSheetFiles()
  {
    return $this -> belongsToMany('App\File')->withPivot('file_type') -> withTimestamps() -> where('file_type', config('tablegamers.game_system_default_id'));
  }

  public function files()
  {
    return $this -> belongsToMany('App\File')->withPivot('file_type') -> withTimestamps() -> where('file_type', '<>', config('tablegamers.game_system_default_id'));
  }

  public function character(){
    return $this -> belongsToMany('App\Character')->withPivot('file_type') -> withTimestamps();
  }
}
