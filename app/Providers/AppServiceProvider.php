<?php

namespace App\Providers;

use App\Character;
use App\Observers\CharacterObserver;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Bugfix "key too long"
        Schema::defaultStringLength(191);
        
        Character::observe(CharacterObserver::class);

        \Carbon\Carbon::setLocale(config('app.locale'));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
