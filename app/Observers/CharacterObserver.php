<?php

namespace App\Observers;

use App\Character;

class CharacterObserver
{
    public function created(Character $character)
    {
        // set all other characters to inactive
        // when there is a new entrythat should be active
        $this -> setActiveCharacter( $character );
    }

    public function updated(Character $character)
    {
        $this -> setActiveCharacter( $character );
    }

    private function setActiveCharacter( Character $character )
    {
     if( $character -> is_active )
      {
        Character::where('group_id', $character -> group_id)
          -> where('user_id', $character -> user_id)
          -> where('id', '<>', $character -> id) 
          -> update(['is_active' => false]);
      } 
    }
}