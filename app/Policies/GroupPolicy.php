<?php

namespace App\Policies;

use App\Group;
use App\GroupInvitation;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupPolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can view the group.
   *
   * @param  \App\User  $user
   * @param  \App\Group  $group
   * @return mixed
   */
  public function view(User $user, Group $group)
  {
    $hasInvitation = !empty( GroupInvitation::where('email', $user -> email)->where('group_id', $group->id)->first() );
    return $hasInvitation || !$group -> is_private || ( $group -> is_private && $group -> users -> contains('id', auth()->id()) );
  }

  /**
   * Determine whether the user can edit, update, delete
   *
   * @param  \App\User  $user
   * @param  \App\Group  $group
   * @return mixed
   */
  public function edit(User $user, Group $group)
  {
    return $user -> isGroupAdmin($group->id);
  }

  public function deteleUser(User $user, Group $group, User $modelUser)
  {
    return $user -> isGroupAdmin($group->id) || $user -> id == $modelUser -> id;
  }

}
