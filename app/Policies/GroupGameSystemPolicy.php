<?php

namespace App\Policies;

use App\Group;
use App\GroupGameSystem;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupGameSystemPolicy
{
  use HandlesAuthorization;

  /**
   * Determine whether the user can update the group game system.
   *
   * @param  \App\User  $user
   * @param  \App\GroupGameSystem  $groupGameSystem
   * @return mixed
   */
  public function update(User $user, GroupGameSystem $groupGameSystem, Group $group)
  {
    return $user -> isGroupAdmin($group->id);
  }

}
