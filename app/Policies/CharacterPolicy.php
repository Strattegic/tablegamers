<?php

namespace App\Policies;

use App\User;
use App\Character;
use Illuminate\Auth\Access\HandlesAuthorization;

class CharacterPolicy
{
  /**
   * Determine whether the user can view the character.
   *
   * @param  \App\User  $user
   * @param  \App\Character  $character
   * @return mixed
   */
  public function view(User $user, Character $character)
  {
    return $this -> canView($user, $character);
  }

  /**
   * Determine whether the user can create characters.
   *
   * @param  \App\User  $user
   * @return mixed
   */
  public function create(User $user, User $modelUser)
  {
    return $user->id == $modelUser->id;
  }

  /**
   * Determine whether the user can update the character.
   *
   * @param  \App\User  $user
   * @param  \App\Character  $character
   * @return mixed
   */
  public function update(User $user, Character $character)
  {
    return $this -> canEdit($user, $character);
  }

  /**
   * Determine whether the user can delete the character.
   *
   * @param  \App\User  $user
   * @param  \App\Character  $character
   * @return mixed
   */
  public function delete(User $user, Character $character)
  {
    return $this -> canEdit($user, $character);
  }

  /**
   * Determine whether the user can restore the character.
   *
   * @param  \App\User  $user
   * @param  \App\Character  $character
   * @return mixed
   */
  public function restore(User $user, Character $character)
  {
    return $this -> canEdit($user, $character);
  }

  /**
   * Determine whether the user can permanently delete the character.
   *
   * @param  \App\User  $user
   * @param  \App\Character  $character
   * @return mixed
   */
  public function forceDelete(User $user, Character $character)
  {
    return $this -> canEdit($character);
  }

  private function canView($user, $character)
  {
    return $user -> isGroupAdmin($character->group->id) || auth()->id() == $character -> user_id || $character -> is_active;
  }

  private function canEdit($user, $character)
  {
    if( empty( $character ) )
    {
      return auth()->id() == $user -> id;
    }
    return auth()->id() == $character -> user_id;
  }
}
