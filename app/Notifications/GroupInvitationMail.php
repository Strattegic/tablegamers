<?php

namespace App\Notifications;

use App\GroupInvitation;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class GroupInvitationMail extends Notification
{
    use Queueable;

    protected $invitation;
    protected $inviterName;
    protected $groupName;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( GroupInvitation $invitation, $inviterName, $groupName )
    {
        $this -> invitation = $invitation;
        $this -> inviterName = $inviterName;
        $this -> groupName = $groupName;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $groupUrl = route('group.show', [$this -> invitation -> group_id, 'groupName' => $this -> groupName, 'inviterName' => $this -> inviterName]);

        $greeting = "Hey";
        if( !empty($this -> invitation -> name) ){
            $greeting .= " " . $this -> invitation -> name . "!";
        }
        else{
            $greeting .= "!";
        }

        return (new MailMessage)
                    ->subject(__('email.invitation.subject', ['name' => $this -> inviterName]))
                    ->from('info@tablegamers.net', 'Table Gamers')
                    ->greeting($greeting)
                    ->line(__('email.invitation.line_1', ['name' => $this -> inviterName, 'groupName' => $this -> groupName]))
                    ->line(__('email.invitation.line_2'))
                    ->action(__('email.invitation.action_button'), $groupUrl)
                    ->line(__('email.invitation.line_3'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
