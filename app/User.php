<?php

namespace App;

use App\Group;
use App\Player;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameAttribute($name)
    {
      return ucfirst( $name );
    }

    public function groups()
    {
        return $this -> belongsToMany('App\Group')->withPivot('is_admin') -> withTimestamps();
    }

    public function characters()
    {
        return $this -> hasMany('App\Character');
    }

    public function characterByGroup(Group $group)
    {
      return $this -> hasMany('App\Character') -> where('group_id', $group->id);
    }

    public function invitations()
    {
        return $this -> hasMany('App\GroupInvitation', 'email', 'email');
    }

    public function getImageAttribute($value)
    {
      if( empty( $value ) )
      {
        return asset( config('tablegamers.user_placeholder_image') );
      }

      return Storage::url($value);
    }

    /**
     * Determines if the user is the group leader for the specified group
     * @var $groupId
     */
    public function isGroupAdmin( $groupId )
    {
        $group = $this -> groups -> where('id', $groupId ) -> first();
        
        if(!empty($group))
        {
            return $group -> pivot -> is_admin;
        }
        return false;
    }
}
