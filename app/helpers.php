<?php

if (! function_exists('get_route_params_with_identifier')) {

	/**
	*	Adds the player identifier to the given route
	*/
    function get_route_params_with_identifier($params)
    {
    	$params[config('companion.player_identifier_key_short')] = Session::get('player_identifier');
    	return $params;
    }
}

if (! function_exists('route_with_identifier')) {

	/**
	*	Adds the player identifier to the given route
	*/
    function route_with_identifier($routeName, $params = null)
    {
        if( Session::get('player_identifier') != null )
        {
    	   $params[config('companion.player_identifier_key_short')] = Session::get('player_identifier');
        }
    	$route = route($routeName, $params);
        return $route;
    }
}