<?php

namespace App\Models\Misc;

class GroupProgress
{
  protected $group;
  protected $user;

  public $isAdmin;
  public $percentage;
  public $messages;

  public function calculate($group, $user, $isAdmin)
  {
    $this -> group = $group;
    $this -> user = $user;
    $this -> isAdmin = $isAdmin;
    $this -> messages = collect();

    if( $isAdmin )
    {
      $this -> allPlayersHaveCharacter();
      $this -> nextSessionDefined();
      $this -> gameSystemDefined();
    }
    else
    {
      $this -> hasAnActiveCharacter();
    }

    $this -> calcPercentage();
  }

  protected function allPlayersHaveCharacter()
  {
    $playerCount = 0;
    $characterCount = 0;

    $this -> group -> users -> where('is_admin', false) -> each(function($user) use (&$characterCount, &$playerCount){
      if( !$user -> pivot -> is_admin )
      {
        $playerCount++;
        $characterCount += $user -> characters -> where('is_active', true) -> count();
      }
    });

    $message = new GroupProgressMessage();
    $message -> isReady = $playerCount <= $characterCount;
    $message -> text = __('group.progress.all_players_have_characters', ['playerCount' => $playerCount, 'characterCount' => $characterCount, 'link' => route('group.character', [$this->group, $this->user])]);
    $this -> messages -> push( $message );
  }

  protected function nextSessionDefined()
  {
    $message = new GroupProgressMessage();
    if(!empty($this -> group -> next_session_datetime) && !empty($this -> group -> next_session_location))
    {
      $message -> isReady = true;
    }
    
    $message -> text = __('group.progress.next_session', ['link' => route('group.edit', [$this->group])]);
    $this -> messages -> push( $message );

  }

  protected function gameSystemDefined()
  {
    if( $this -> group -> system -> is_custom_system && 
      empty( $this -> group -> system -> custom_name ) )
    {
      $message = new GroupProgressMessage();
      $message -> text = __('group.progress.custom_system_name', ['link' => route('group.edit', [$this->group])."#system"]);
      $this -> messages -> push( $message );
    }

    if( $this -> group -> system -> is_custom_system && 
      empty( $this -> group -> system -> custom_file_id ) )
    {
      $message = new GroupProgressMessage();
      $message -> text = __('group.progress.custom_system_character_sheet', ['link' => route('group.edit', [$this->group])."#system"]);
      $this -> messages -> push( $message );
    }
  }

  protected function hasAnActiveCharacter()
  {
    $message = new GroupProgressMessage();

    if(!empty($this -> user -> characterByGroup($this->group) -> first()))
    {
      $message -> isReady = true;
    }
    $message -> text = __('group.progress.has_active_character', ['link' => route('group.character', [$this -> group])]);
    $this -> messages -> push( $message );
  }

  protected function calcPercentage()
  {
    $min = 0;
    $max = $this -> messages -> count();
    foreach( $this -> messages as $message )
    {
      if($message -> isReady)
      {
        $min++;
      }
    }

    if( $max > 0 )
    {
      $this -> percentage = ($min * 100) / $max;
    }
    else
    {
      $this -> percentage = 0;
    }

    $this -> percentage = round($this -> percentage);
  }
}
