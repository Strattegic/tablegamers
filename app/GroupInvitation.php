<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupInvitation extends Model
{
    use SoftDeletes;

    protected $fillable = ['group_id', 'name', 'email', 'inviter_user_id'];
    protected $dates = ['deleted_at'];

    public function group()
    {
    	return $this -> belongsTo('\App\Group');
    }

    public function inviter()
    {
      return $this -> belongsTo('\App\User', 'inviter_user_id', 'id');
    }
}
