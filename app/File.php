<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
  use SoftDeletes;
  
  protected $table = 'files';
  protected $appends = ['size'];

  protected $fillable = ['name', 'path', 'mime_type', 'uploader_user_id'];

  public function group(){
    return $this -> hasOne('App\Group', 'id', 'game_system_file_id');
  }

  public function character(){
    return $this -> belongsToMany('App\Character')->withPivot('file_type') -> withTimestamps();
  }

  public function getSizeAttribute()
  {
    return $this -> bytesToHuman(Storage::disk('public')->size($this->path));
  }

  // TODO: relocate this function to its own helper class
  public static function bytesToHuman($bytes)
  {
    $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

    for ($i = 0; $bytes > 1024; $i++) {
      $bytes /= 1024;
    }

    return round($bytes, 2) . ' ' . $units[$i];
  }
}
