<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Home > Group
Breadcrumbs::for('group.show', function ($trail, $group) {
    $trail->parent('home');
    $trail->push($group -> name, route('group.show', ['groupId' => $group -> id]));
});

// Home > Group.create
Breadcrumbs::for('group.create', function ($trail) {
    $trail->parent('home');
    $trail->push("Create a new group", route('group.create'));
});

// Home > Group.edit
Breadcrumbs::for('group.edit', function ($trail, $group) {
    $trail->parent('group.show', $group);
    $trail->push(__('group.edit_group'), route('group.edit', [$group->id]));
});

// Home > Group > File.index
Breadcrumbs::for('group.file.index', function ($trail, $group) {
    $trail->parent('group.show', $group);
    $trail->push(__('group.files'), route('group.file.index', ['groupId' => $group->id]));
});

// User
Breadcrumbs::for('user.show', function ($trail, $user) {
    $trail->push('User profile', route('user.show', ['userId' => $user -> id]));
});

    // Home > Group > Characters (all main characters for each user)
    Breadcrumbs::for('group.characters', function ($trail, $group) {
        $trail->parent('group.show', $group);
        $trail->push(__('group.menu_characters'), route('group.characters', ['groupId' => $group->id]));
    });

    // Home > Group > User.index (all users of this group)
    Breadcrumbs::for('group.user.index', function ($trail, $group) {
        $trail->parent('group.show', $group);
        $trail->push(__('group.menu_users'), route('group.user.index', ['groupId' => $group->id]));
    });

    // Home > Group > User.show (specific user)
    Breadcrumbs::for('group.user.show', function ($trail, $group, $user) {
        $trail->parent('group.show', $group);
        $trail->push($user->name, route('group.user.show', [$group->id, $user->id]));
    });

    // Home > Group > Character.index
    Breadcrumbs::for('character.index', function ($trail, $group, $user) {
        $trail->parent('group.show', $group);
        $trail->push($user->name);
        $trail->push(__('group.menu_characters'), route('character.index', [$group->id, $user->id]));
    });

    // Home > Group > Character.show
    Breadcrumbs::for('character.show', function ($trail, $group, $user, $character) {
        $trail->parent('group.show', $group);
        $trail->push(__('group.menu_characters'), route('character.index', [$group -> id, $user -> id]));
        $trail->push($character -> name, route('character.show', [$group -> id, $user -> id, $character -> id]));
    });

    // Home > Group > Character.create
    Breadcrumbs::for('character.create', function ($trail, $group, $user) {
        $trail->parent('group.show', $group);
        $trail->push(__('group.characters'), route('character.index', [$group -> id, $user -> id]));
        $trail->push(__('group.character_create'), route('character.create', [$group->id, $user -> id]));
    });