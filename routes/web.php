<?php

// determine the language
$lang = Request::server('HTTP_ACCEPT_LANGUAGE');
if( isset( explode(',', $lang)[0] ) )
{
	App::setLocale( explode(',', $lang)[0] );
}

App::setLocale('de');

Auth::routes();

// Player
// the player does not have to be authorized to access the character sheet and group
Route::group(['middleware' => ['playerAccess']], function() {
	// TODO: were player specific views -> now user specific
	// Route::resource('group/{group}/player/{player}/file', 'Companion\PlayerFileController');
	// Route::resource('group/{group}/player/{player}/character', 'Companion\CharacterController');
	// Route::post('group/{group}/player/{player}/character/{characterId}/set-active', 'Companion\CharacterController@setActive') -> name('character.make_active');
});

// HomeController determines which view should be rendered
Route::get('/', 'HomeController@index') -> name('home');


// all frontend routes
Route::group( [], function(){
  Route::get('register', function(){ 
    return view('public.register'); 
  }) -> name('register');
	Route::get('privacy-policy', function(){
		return view('public.privacy_policy');
	}) -> name('privacy-policy');
	Route::get('imprint', function(){
		return view('public.imprint');
	}) -> name('imprint');
	Route::get('features', 'HomeController@showFeatures');
});


// all backend routes for the companion app are protected
Route::group(['middleware' => ['auth']], function () {

	// Logout
	Route::get('logout', function(){
		Auth::logout();
		return redirect('/');
	}) -> name('logout');

  Route::resource('user', 'Companion\UserProfileController');
  Route::post('user/{user}/changePassword', 'Companion\UserProfileController@changePassword') -> name('user.change_password');
	

  Route::get('files/{file}/download', 'Companion\FileController@download') -> name('files.download');
  Route::delete('files/{file}', 'Companion\FileController@destroy') -> name('files.destroy');
	
  // Group
  Route::resource('group', 'Companion\GroupController');

  // Group.* routes
  Route::name('group.')->middleware('can:view,group')->group( function(){

    // Group
		Route::post('group/{group}/invite-user', 'Companion\GroupController@inviteUser') -> name('invite_user');
		Route::post('group/{group}/remove-player', 'Companion\GroupController@removePlayer') -> name('remove_player');

    Route::resource('group/{group}/game-system', 'Companion\GroupGameSystemController') -> only(['update']);

    // TODO!!!
    // accept and reject need to be POST methods!
    Route::name('invitation.')->group( function() {
      Route::get('group/{group}/invitation/{groupInvitation}/accept', 'Companion\GroupInvitationController@accept')->name('accept');
  		Route::get('group/{group}/invitation/{groupInvitation}/reject', 'Companion\GroupInvitationController@reject')->name('reject');
      Route::delete('group/{group}/invitation/{groupInvitation}/destroy', 'Companion\GroupInvitationController@destroy')->name('destroy');
    });

    // Group Users
    // Route::resource('group/{group}/character', 'Companion\GroupCharacterController');
    Route::resource('group/{group}/user', 'Companion\GroupUserController') -> only(['show', 'index', 'destroy']);

    Route::get('group/{group}/characters', 'Companion\GroupController@showCharacters') -> name('characters');

    Route::get('group/{group}/user/{user}/character', 'Companion\GroupCharacterController@show') -> name('character');
    Route::put('group/{group}/user/{user}/character', 'Companion\GroupCharacterController@update') -> name('character.update');

    // Characters
    Route::name('character.')->group( function() {

      // Characters
      Route::resource('group/{group}/character/{character}/file', 'Companion\GroupCharacterFileController') -> only(['store', 'show', 'destroy']);
      Route::name('file.')->group( function() {
        // Character Files
        Route::get('group/{group}/character/{character}/file/{file}/download', 'Companion\GroupCharacterFileController@download') -> name('download');
      });

    });

  });
	

	// User Settings
	// Route::resource('user', 'Companion\UserProfileController');

});
