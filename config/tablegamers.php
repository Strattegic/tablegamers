<?php

return [

	'group_placeholder_image' => 'img/group_placeholder.png',
  'character_placeholder_image' => '/img/character_placeholder.jpg',
	'user_placeholder_image' => '/img/character_placeholder.jpg',

  'group_images_folder' => 'group_images',
	'group_character_sheet_folder' => 'group_character_sheet',

  'game_system_default_id' => 0,
  'game_system_default_file_name' => 'group.character_sheet',

  'file_types' => [
    'character_sheet' => 'character_sheet',
  ],

];