## About Table Gamers

Table Gamers is a website that wants to make playing boardgames and pen & paper games easier. For a better offline or online playing experience.

Table Gamers attempts to make planning your game, finding a group and managing ongoing groups easier.

## License

Table Gamers is open-sourced software licensed under the [GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0).
