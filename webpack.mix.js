let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var tailwindcss = require('tailwindcss');

mix.js('resources/assets/js/main.js', 'public/js')
   .sass('resources/assets/sass/public.scss', 'public/css')
   .sass('resources/assets/sass/companion/companion.scss', 'public/css')
  .scripts([
    'resources/assets/js/functions.js'
  ], 'public/js/all.js');

mix.copyDirectory('node_modules/@fortawesome/fontawesome-free-webfonts/webfonts', 'public/webfonts');