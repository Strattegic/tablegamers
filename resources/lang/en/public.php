<?php

return [
  
  'brand_subtitle' => 'Your site for organizing your Pen & Paper groups.',

  'already_have_an_account' => 'Already have an account? please <a href=":link">Sign In</a>',
  'accept_privacy_text' => 'By creating an account you agree to the <a href=":link">Privacy Policy</a>.',
  'dont_have_an_account' => 'Don\'t have an account? <a href=":link">Sign Up</a>',

  'register' => 'Register',
  'username' => 'Username',
  'name_help_text' => 'This name will be shown to other users.',
  'email' => 'E-Mail',
  'password' => 'Password',
  'password_confirmation' => 'Password (Confirmation)',
  'forgot_password' => '<a href=":link">Forgot Password?</a>',

  'login' => 'Login',

  'footer_text' => '© 2018 All Rights Reserved. <a href=":link_privacy_policy">Privacy</a> and <a href=":link_imprint">Terms</a>',
];