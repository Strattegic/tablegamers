<?php

return [
	'app_title' => 'Table Gamers - tables and dice',

	'group' => 'Group',
	'leader' => 'Group leader',
	'leader_short' => 'Leader',
	'game_type' => 'Game type',

	'modal_cancel' => 'cancel',
  
  'main_nav' => [
    'dashboard' => 'Home',
    'my_groups' => 'Meine Gruppen',
    'no_groups_yet' => 'You have not created or joined a group yet.',
    'groups_as_admin' => 'Als Gruppenleiter',
    'groups_as_player' => 'Als Spieler',
  ],

  'notifications' => [
    'no_notifications' => 'No new notifications',
    'all_notifications' => 'show all notifications',
    'pending_invite' => 'You have a pending invitation to join the group <strong>:name</strong>',
    'pending_invite_go_to_group' => 'Go to group',
  ],

  'privacy_policy' => 'Privacy Policy',
  'imprint' => 'Imprint',
];