<?php

return [
	'invitation' => [
		'subject' => ':name has invited you to a Table Gamers group',
		'line_1' => ':name has invited you to join the group ":groupName"',
		'line_2' => 'To join, just click on the button below. There you will be able to fill in your character sheet.',
		'action_button' => 'Join Group',
		'line_3' => 'Have fun, enjoy playing, and thank you for using my application'
	]
];