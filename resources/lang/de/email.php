<?php

return [
	'invitation' => [
		'subject' => ':name hat dich zu einer Table Gamers Gruppe eingeladen',
		'line_1' => ':name hat dich eingeladen der Gruppe ":groupName" beizutreten',
		'line_2' => 'Um teilzunehmen, klicke einfach auf den Button. Auf der nachfolgenden Seite kannst du deinen Charakterbogen erstellen.',
		'action_button' => 'Gruppe beitreten',
		'line_3' => 'Viel Spaß, fröhliches Spielen und danke dass du meine Seite benutzt. - Mathias @ Table Gamers'
	]
];