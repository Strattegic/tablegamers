<?php

return [
	'your_groups' => 'Deine Gruppen',
	'your_groups_as_player' => 'Gruppen in denen du mitspielst',
  'no_groups_yet' => 'Du hast noch keine Gruppen erstellt.',
  'create_new_group' => 'Erstelle eine Gruppe',
	'next_session' => 'Nächster Spieltermin',
  'next_session_time' => 'Zeit',
  'next_session_location' => 'Ort',
	'next_session_time_not_set' => 'n/a',
	'next_session_location_not_set' => 'n/a',
  'visibility' => 'Sichtbarkeit',
  'visibility_status' => [
    'private' => 'privat',
    'public' => 'öffentlich',
  ],
  'private_group' => [
    'message' => 'Dies ist eine private Gruppe. <br>Du benötigst eine Einladung des Gruppenleiters um dieser Gruppe beizutreten.',
    'back_to_my_groups' => 'Meine Gruppen',
  ],
  'progress' => [
    'title_admin' => 'Deine Aufgaben <small class="text-muted">(Gruppenleiter) </small>',
    'subtitle_admin' => 'Mit dieser Checkliste kannst du sicherstellen, dass du alle benötigten Informationen hast bevor du mit deiner ersten Pen & Paper Runde anfängst.',
    'title_player' => 'Deine Aufgaben vor dem Spielstart',
    'all_players_have_characters' => 'Jeder Spieler hat einen Hauptcharakter <br>
          <small class="text-muted">
            Du brauchst <em>nur</em> einen Charakter wenn du auch selber spielen möchtest.
          </small>',
    'next_session' => 'Lege Termin und Ort für die nächste Spielrunde fest.<br><small class="text-muted"><a href=":link">Zu den Gruppeneinstellungen</a></small>',

    'has_active_character' => 'Erstelle einen neuen Charakter und mache diesen zu deinem Hauptcharakter.<br><small><a href=":link">Zur Charaktererstellung</a></small>',

    'custom_system_name' => 'Gib deinem System einen Namen.<br><small class="text-muted"><a href=":link">Zu den Gruppeneinstellungen</a></small>',
    'custom_system_character_sheet' => 'Lade einen Charakterbogen hoch.<br><small class="text-muted"><a href=":link">Zu den Gruppeneinstellungen</a></small>',
  ],
  'game_system_name' => 'Name des Systems',
  'character_sheet' => 'Charakterbogen',

  'image' => 'Gruppenbild',
  'image_please_select' => 'auswählen...',
  'image_help_text' => '',
  'name' => 'Name',
  'game_system' => 'System / Regelwerk',
  'game_system_short' => 'System',
  'description' => 'Gruppenbeschreibung',
  'description_none' => 'Keine Informationen angegeben',

  'overview_title' => 'Beschreibung',
  'create' => 'erstellen',
	'users' => 'Spieler',
	'admin' => 'Admin',
	'activites' => 'Aktivitäten',
	'activities_no_recent' => 'Keine Aktivitäten in letzter Zeit...',
	'files' => 'Dateien',
	'characters' => 'Charaktere',
	'character_create' => 'erstellen',
	'save' => 'Speichern',
  'save_changes' => 'Änderungen speichern',
  'cancel' => 'Abbrechen',
	'edit_group' => 'Bearbeiten',

  'delete_group' => 'Gruppe löschen',
  'delete_group_text' => '<strong>Achtung!</strong> Wenn du die Gruppe löschst, werden auch alle Charaktere, verknüpfte Dateien und Einstellungen gelöscht.',
  'delete' => 'Löschen',
  'delete_confirmation' => 'Bist du dir sicher, dass du die Gruppe <strong>:name</strong> löschen möchtest? <br>Alle zugehörigen Daten gehen verloren!',

  'users_you' => 'Du',

	'invite_user' => [
		'title' => 'Neuen Spieler einladen',
    'invite_text' => 'Spieler einladen',
    'invite_text_short' => 'Einladen',
		'confirmation' => 'Senden',
		'player_name' => 'Spielername',
		'player_name_tooltip' => 'Wird in der Einladungsmail verwendet.',
		'email' => 'E-Mail',
		'send_at' => 'Eingeladen: :date',
    'invitation_pending' => 'Eingeladen',
	],
  'user_delete_confirmation' => 'Möchtest du wirklich :name aus der Gruppe entfernen?',
  'user_invitation_confirmation' => 'Möchtest du die Einladung für :name wirklich löschen?',

  'menu_my_character' => 'Mein Charakter',
  'menu_group_settings' => 'Gruppeneinstellungen',
  'menu_overview' => 'Überblick',
  'menu_users' => 'Spieler',
  'menu_characters' => 'Charaktere',

  'before_ready_alerts_title' => 'Deine Aufgaben',
  'before_ready_alerts_subtitle' => 'Was du noch tun musst bevor du mit dem Spielen beginnen kannst',
  'before_ready_alerts' => [
    'no_character' => '<a href=":url">Erstelle</a> einen Charakter mit dem du spielen willst.',
  ],
];