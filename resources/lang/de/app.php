<?php

return [
	'app_title' => 'Table Gamers',
	'app_title_addition_player' => ' - :name Spielerprofil',

  'your_groups' => 'Deine Gruppen',
  'create_new_group' => 'Create new group',
  'no_groups_created_yet' => 'You do not have created a game yet.',
  'find_a_group' => 'Find a group',
  'no_groups_joined_yet' => 'You do not have joined a game yet.',

  'invitations' => 'Einladungen',
  'invitations_text' => 'Du wurdest in folgende Gruppen eingeladen.',

  'main_nav' => [
    'dashboard' => 'Home',
    'my_groups' => 'Meine Gruppen',
    'no_groups_yet' => 'Du hast noch keine Gruppen.',
    'groups_as_admin' => 'Als Gruppenleiter',
    'groups_as_player' => 'Als Spieler',
  ],

  'modal' => [
    'confirmation_title' => 'Bist du dir sicher?',
    'confirmation_cancel_button' => 'Abbrechen',
    'confirmation_button' => 'Klar!',
  ],

  'notifications' => [
    'no_notifications' => 'Keine neuen Benachrichtigungen',
    'all_notifications' => 'Zeige alle Benachrichtigungen',
    'pending_invite' => 'Du wurdest in die Gruppe <strong>:name</strong> eingeladen.',
    'pending_invite_this_group' => 'Du wurdest von <strong>:name</strong> eingeladen dieser Gruppe beizutreten.',
    'pending_invite_go_to_group' => 'Zur Gruppe',
  ],

  'game_system' => [
    'default' => [
      'title' => 'Eigenes System',
      'name' => 'Name des Systems',
      'helptext' => 'Diese Auswahl ermöglicht es dir ein System zu spielen, das noch nicht in unserer Datenbank vorhanden ist. Du bestimmst selber wie der Charakterbogen aussehen soll und nach welchen Regeln gespielt wird.',
      'character_sheet_helptext' => 'Lade deinen eigenen Charakterbogen hoch. Deine Spieler bekommen diesen zur Verfügung gestellt und können ihn ausgefüllt hochladen.',
    ],
  ],

  'character_sheet_not_uploaded' => 'Kein Charakterbogen hochgeladen',
  'file' => [
    'modified_at' => 'geändert :time',
    'no_upload_yet' => 'Keine Datei hochgeladen',
    'please_select' => 'Auswählen...',
  ],
  'upload' => 'hochladen',

  'accept' => 'Annehmen',
  'decline' => 'Ablehnen',
  

  'privacy_policy' => 'Datenschutzrichtlinien',
  'imprint' => 'Impressum',
];