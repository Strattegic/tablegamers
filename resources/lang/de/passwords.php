<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwörter müssen mindestens 6 Zeichen lang sein und gleich dem Bestätigungspasswort sein.',
    'reset' => 'Dein Passwort wurde zurückgesetzt!',
    'sent' => 'Wir haben dir eine E-Mail mit dem Link zum Zurücksetzen deines Passworts geschickt!',
    'token' => 'Dieses Formular ist ungültig. Bitte versuche erneut dein Passwort zurückzusetzen.',
    'user' => "Es gibt keinen Nutzer mit dieser E-Mail Adresse.",

];
