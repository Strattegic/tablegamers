<?php
	return [
		'create_character' => 'Charakter erstellen',
		'create' => 'erstellen',
    'delete_confirmation_title' => 'Charakter löschen',
    'delete_confirmation' => 'Willst du den Charakter ":name" wirklich löschen?',
    'delete_confirmation_button' => 'Löschen',
    'character_info' => 'Charakterinformationen',
    'upload_image' => 'Bild hochladen',
    'character_files' => 'Dateien',
    'character_add_file' => 'Datei hinzufügen',
    'no_files_uploaded_yet' => 'Du hast noch keine Dateien zu diesem Charakter hochgeladen.',
  	'character_sheet' => 'Charakterbogen',
    'your_character_sheet' => 'Dein Charakterbogen',
    'your_character_sheet_help_text' => 'Der Charakterbogen stellt die ausführliche Beschreibung deines Charakters dar. Er enthält alles was du zum Spielen in der Gruppe benötigst. Unter anderem sind hier die Status-, Fähigkeits- und/oder Erfahrungspunkte angegeben.<br>Der Spielleiter hat die Möglichkeit eine Vorlage anzu',
    'character_sheet_text_1_no_sheet' => 'Der Spielleiter hat noch keine Vorlage für den Charakterbogen bereitgestellt.',
    'character_sheet_text_1' => 'Der Spielleiter hat eine Vorlage zur Charaktererstellung bereitgestellt. Zuerst musst du diese herunterladen und eine eigene Datei mit deinem Charakter erstellen.',
    'character_sheet_text_2' => 'Nachdem du den Charakterbogen erstellt hast kannst du ihn hier hochladen',

    'character_sheet_none_uploaded' => 'Es wurde noch kein Charakterbogen hochgeladen.',

		'name' => 'Name',
    'user_name' => 'Spieler',
		'gender' => 'Geschlecht',
		'age' => 'Alter',
    'race' => 'Rasse',
    'class' => 'Klasse',
    'description' => 'Beschreibung / Biografie',
    'description_none_set' => '<small><em>Keine Beschreibung angegeben</em></small>',
    'description_short' => 'Kurzbeschreibung',
    'image' => 'Bild',
		'physique' => 'Statur',
		'religion' => 'Religion',
		'profession' => 'Beruf',
		'marital_status' => 'Familienstand',
		'biography' => 'Biografie / Notizen',
		'biography_detail' => 'Hier gibt es Platz für deine Hintergrundgeschichte oder Notizen für den Spielleiter',

		'skills' => 'Fähigkeit|Fähigkeiten',
		'acting' => 'Aktion',
		'knowledge' => 'Wissen',
		'interact' => 'Interagieren',

		'base_information' => 'Basisinformationen',

		'sheet_description' => '<p>Der Charakterbogen stellt deinen Charakter im Spiel dar. Für ein möglichst cooles Spielerlebnis ist es wichtig dass du dir einen Charakter überlegst den du auch wirklich spielen willst. Ob er/sie für die Gruppe nützlich ist sollte euch erstmal nicht interessieren. Viel wichtiger ist ob ihr euch in den Charalter hineindenken könnt.</p>
    		<p>Für diese Runde brauchst du einen Charakter der in das Call of Cthulhu Universum passt. Hierbei ist dir völlig frei gelassen ob du nun einen Metzger aus London, eine Hausfrau aus New York oder den Zeitungsjungen aus Arkham spielst. Der Charakter sollte einfach in das Setting passen.</p>',

      	'sheet_skills_description' => '<p>Du hast insgesamt 400 Punkte zur Verfügung die du in alle erdenklichen Fähigkeiten investieren kannst. Diese sollten deinen Charakter spezialisieren und seine/ihre herausragendsten Eigenschaften zur Geltung bringen. Es werden mit Absicht keine Beispiele für Fähigkeiten genannt da damit deine Kreativität eingeschränkt werden würde.</p>
		      <ul>
		        <li>Aktion: Welche besonderen ausführbaren körperlichen Fähigkeiten hat dein Charakter?</li>
		        <li>Wissen: Welches Fachwissen besitzt dein Charakter?</li>
		        <li>Interagiern: In welchen sozialen Interaktionen ist dein Charakter besonders gut?</li>
		      </ul>
		      <p>Für jeden Punkt den du in eine Fähigkeit investierst werden 10% auf die Kategorie angerechnet. Wenn du einer Fähigkeit mehr als 80 Punkte zuweist wird es eine <span class="has-text-primary">Meisterfähigkeit</span>. Diese geben noch einmal 10 Extra Punkte auf die Kategorie. Die Kategorien sind dann wichtig wenn du im Spiel eine Fähigkeit ausführen musst die du gar nicht "geskillt" hast. Dann wird auf den Wert der Kategorie zurück gegriffen. Das heißt dass ein Charakter der sehr viele Punkte in körperliche Fähigkeiten investiert von Natur aus sehr geübt ist und die meisten körperlichen Aktivitäten durchschnittlich besser erledigt als andere.</p>',

		'save_changes' => 'Änderungen speichern',
		'delete' => 'Löschen',
		'delete_character' => 'Charakter Löschen',
		'delete_warning' => '<p>Möchtest du den Charakter <strong>:name</strong> wirklich löschen? <br>Dieser Schritt kann nicht rückgängig gemacht werden!</p>',
		'print' => 'Drucken',
		'set_active' => 'Als Hauptcharakter markieren',
    'active_character' => 'Hauptcharakter',
		'active_characters' => 'Hauptcharaktere',
    'no_character_yet' => 'Noch keine Charaktere angelegt',
    'no_character_yet_text' => '<p>Du hast noch keinen Charakter erstellt...</p>',
	];
?>