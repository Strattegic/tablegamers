<?php

return [
  'username' => 'Nutzername',
  'name_help_text' => 'Andere Nutzer werden dich an diesem Namen erkennen.',
  'email' => 'E-Mail',
  'password' => 'Passwort',

  'settings' => 'Einstellungen',
  'reset_password' => 'Passwort ändern',
  'current_password' => 'Aktuelles Passwort',
  'new_password' => 'Neues Passwort',
  'new_password_confirmation' => 'Neues Passwort (Bestätigung)',

  'settings_changed_success' => 'Einstellungen erfolgreich geändert',
  'password_reset_success' => 'Passwort erfolgreich geändert!',

  'logout' => 'Abmelden',
];