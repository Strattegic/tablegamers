<?php

return [

  'brand_subtitle' => 'Dein Tool für ein besseres Pen & Paper Erlebnis.',

  'already_have_an_account' => 'Du hast einen Account? Einfach <a href=":link">einloggen</a>.',
  'accept_privacy_text' => 'Mit der Erstellung eines Accounts stimmst du zu, dass du die <a href=":link">Datenschutzrichtlinien</a> gelesen hast.',
  'dont_have_an_account' => 'Du hast noch keinen Account? <a href=":link">Erstell einfach einen</a>.',

  'register' => 'Account erstellen',
  'username' => 'Nutzername',
  'name_help_text' => 'Andere Nutzer werden dich an diesem Namen erkennen.',
  'email' => 'E-Mail',
  'password' => 'Passwort',
  'password_confirmation' => 'Passwort (Bestätigung)',
  'forgot_password' => '<a href=":link">Passwort vergessen?</a>',
  'password_reset' => 'Passwort zurücksetzen',
  'password_reset_text' => 'Du kannst jetzt ein neues Passwort vergeben. Bitte gib zur Sicherheit noch einmal deine E-Mail Adresse ein.',

  'forgot_password_title' => 'Passwort Vergessen',
  'forgot_password_send_request' => 'Passwort zurücksetzen',

  'login' => 'Login',

  'footer_text' => '© 2018 - <a href=":link_privacy_policy">Datenschutzrichtlinien</a> - <a href=":link_imprint">Impressum</a>',
  'privacy_policy' => 'Datenschutzerklärung',
  'imprint' => 'Impressum',
  'back' => 'zurück',

  'errors' => [
    '404' => [
      'title' => '404',
      'message' => '<p>Die angeforderte Seite konnte nicht gefunden werden!</p><a href=":link" class="btn btn-outline-primary">Zurück</a>',
    ],
    '403' => [
      'title' => '403',
      'message' => '<p>Dir fehlt leider die notwendige Berechtigung!</p><a href=":link" class="btn btn-outline-primary">Zurück</a>',
    ],
  ],
];  