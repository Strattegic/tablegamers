function updatePreview( element, targetId )
{
  updatePreviewImage(element, targetId, false);
}

function updatePreviewBackground( element, targetId )
{
  updatePreviewImage(element, targetId, true);
}

function updatePreviewImage(element, targetId, backgroundImage)
{
  var file = element.files[0];
  var target = document.getElementById(targetId);
  var reader = new FileReader();

  if( backgroundImage )
  {
    reader.onloadend = function () {
      target.style.backgroundImage = "url("+reader.result+")";
    }
  }
  else
  {
    reader.onloadend = function () {
      target.src = reader.result;
    }
  }

  if (file) {
    reader.readAsDataURL(file);
  }
}

function changedGameSystem(element, id)
{
  document.getElementById(id).style.display = 'none';
  if( element.value == 0 )
  {
    document.getElementById(id).style.display = 'block';
  }
}