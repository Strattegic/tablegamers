@extends('layouts.public')

@section('content')
  <form class="auth-form" method="POST" action="{{ route('password.request') }}">
    @csrf
    <input type="hidden" name="token" value="{{$token}}">
    <p class="pb-2">@lang('public.password_reset_text')</p>
    <div class="form-group">
      <div class="form-label-group">
        <input type="email" name="email" class="form-control placeholder-shown" placeholder="@lang('public.email')" value="{{ $email or old('email') }}" required autofocus>
      </div>
    </div>
    <div class="form-group">
      <div class="form-label-group">
        <input type="password" name="password" class="form-control placeholder-shown" placeholder="@lang('public.password')" required>
      </div>
    </div>
    <div class="form-group">
      <div class="form-label-group">
        <input type="password" name="password_confirmation" class="form-control placeholder-shown" placeholder="@lang('public.password_confirmation')" required>
      </div>
    </div>
    @include('shared.errors')
    <div class="form-group">
      <button class="btn btn-lg btn-primary btn-block" type="submit">@lang('public.password_reset')</button>
    </div>
  </form>
@endsection
