@extends('layouts.public')

@section('content')
  <form class="auth-form" method="POST" action="{{ route('password.email') }}">
    @csrf
    <!-- <h5 class="pb-2">@lang('public.forgot_password_title')</h5> -->
    <div class="form-group">
      <div class="form-label-group">
        <input type="email" name="email" class="form-control placeholder-shown" placeholder="@lang('public.email')" value="{{ old('email') }}" required autofocus>
      </div>
    </div>
    @include('shared.errors')
    @if (session('status'))
      <div class="alert alert-success">
        {{ session('status') }}
      </div>
    @endif
    <div class="form-group">
      <button class="btn btn-lg btn-primary btn-block" type="submit">@lang('public.forgot_password_send_request')</button>
    </div>
  </form>
@endsection
