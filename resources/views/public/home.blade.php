@extends('layouts.public')
@section('content')
<section class="content auth">
  <form method="POST" action="{{ route('login') }}">
    @csrf
    <p class="pb-2">@lang('public.dont_have_an_account', ['link' => route('register')])</p>
    <div class="form-group">
      <div class="form-label-group">
        <input type="email" name="email" class="form-control placeholder-shown" placeholder="@lang('public.email')" value="{{old('email')}}" required="" autofocus="">
      </div>
    </div>
    <div class="form-group">
      <div class="form-label-group">
        <input type="password" name="password" class="form-control placeholder-shown" placeholder="@lang('public.password')" required="">
      </div>
    </div>
    <div class="form-group">
      <button class="btn btn-lg btn-primary btn-block" type="submit">@lang('public.login')</button>
    </div>
    <p class="text-center text-muted mb-0">@lang('public.forgot_password', ['link' => route('password.request')])</p>
  </form>
</section>
@endsection