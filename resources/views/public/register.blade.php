@extends('layouts.public')
@section('content')
<section class="content auth">
  <form method="POST" action="{{ route('register') }}">
    @csrf
    <p class="pb-2">@lang('public.already_have_an_account', ['link' => route('home')])</p>
    <div class="form-group">
      <div class="form-label-group">
        <input type="email" name="register_email" value="{{ old('register_email') }}" class="form-control placeholder-shown" placeholder="@lang('public.email')" required="" autofocus="">
      </div>
    </div>
    <div class="form-group">
      <div class="form-label-group">
        <input type="text" name="register_name" value="{{ old('register_name') }}" class="form-control placeholder-shown" placeholder="@lang('public.username')" required="">
        <small class="form-text text-muted">@lang('public.name_help_text')</small>
      </div>
    </div>
    <div class="form-group">
      <div class="form-label-group">
        <input type="password" name="register_password" value="{{ old('register_password') }}" class="form-control placeholder-shown" placeholder="@lang('public.password')" required="">
      </div>
    </div>
    @include('shared.errors')
    <div class="form-group">
      <button class="btn btn-lg btn-primary btn-block" type="submit">@lang('public.register')</button>
    </div>
    <p class="text-center text-muted mb-0"> @lang('public.accept_privacy_text', ['link' => route('privacy-policy')])</p>
  </form>
</section>
@endsection