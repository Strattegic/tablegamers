@extends('layouts.public')
@section('content')
<section class="container content pb-3">
  <a href="{{route('home')}}">@lang('public.back')</a>
  <h1>@lang('public.privacy_policy')</h1>
  @lang('privacy_policy.text')
</section>
@endsection