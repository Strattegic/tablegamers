@extends('layouts.companion')
@section('body')
  @parent
  <div class="container content pt-4">
    @yield('content')
  </div>
@endsection