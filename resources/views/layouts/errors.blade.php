@extends('layouts.app')
@section('head')
  <link href="{{ asset('css/public.css') }}" rel="stylesheet">
@endsection
@section('main')
  <div id="app" class="main-wrapper">
    <section class="container error d-flex p-4 align-items-center justify-content-center">
      <div class="text-center">
        <h1 class="text-muted">@yield('title')</h1>
        @yield('message')
      </div>
    </section>
  </div>
@endsection