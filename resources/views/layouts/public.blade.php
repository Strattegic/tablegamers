@extends('layouts.app')
@section('head')
  <link href="{{ asset('css/public.css') }}" rel="stylesheet">
@endsection
@section('main')
<div id="app" class="main-wrapper">

  <header class="auth">
    <h1 class="display-4 brand-font">
      <span class="text-primary">table</span><span class="text-dark">gamers</span>
    </h1>
    <p class="font-weight-normal">@lang('public.brand_subtitle')</p>
  </header>
  
  @yield('content')

  <footer class="footer text-muted"> 
    @lang('public.footer_text', ['link_imprint' => route('imprint'), 'link_privacy_policy' => route('privacy-policy')])
  </footer>
</div>
@endsection