@extends('layouts.companion')
@section('body')

@if( empty( $pendingInvitation ) )
  <div id="app" class="pt-4 bg-white align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="preview-image" style="background-image: url('{{ $group -> image }}');"></div>
        </div>
        <div class="col-md-6">
          <h2 class="mb-0">{{ $group -> name }}</h2>
          <h5 class="text-muted">
            @if(empty($group -> game_system_name))
              <em>@lang('app.game_system.default.title')</em>
            @else
              {{ $group -> game_system_name . ' ('.__('app.game_system.default.title').')' }}
            @endif
          </h5>
          <ul class="list-inline">
            <!-- <li class="list-inline-item">Göttingen, Deutschland</li> -->
            <!-- <li class="list-inline-item"><a href="{{ route('group.user.index', [$group->id]) }}">{{ sizeof($group->users) }} Player(s)</a></li> -->
          </ul>
          <dl>
            <dt class="mt-2">@lang('group.next_session')</dt>
            <dd>
              <i class="far fa-clock"></i>
                @if( auth()->user() -> isGroupAdmin($group->id) )
                <a href="{{ route('group.edit', [$group]) }}">
                @else
                <a>
                @endif
                  @if($group -> next_session_datetime != null)
                    {{ $group -> next_session_datetime -> toFormattedDateString() }}
                  @else
                    @lang('group.next_session_time_not_set')
                  @endif
                </a>
              <br>
              <i class="far fa-compass"></i> 
                @if( auth()->user() -> isGroupAdmin($group->id) )
                <a href="{{ route('group.edit', [$group]) }}">
                @else
                <a>
                @endif
                  @if( $group -> next_session_location )
                    {{ $group -> next_session_location }}
                  @else
                    @lang('group.next_session_location_not_set')
                  @endif
                </a>
            </dd>
          </dl>
          <p><strong>@lang('group.visibility'):</strong> @lang('group.visibility_status.private')</p>
        </div>
      </div>
    </div>

    <hr>

    <div class="container">
      <nav id="group" class="nav mb-3 w-100">
        <a class="nav-link {{ request()->routeIs('group.show') ? 'active' : '' }} pl-0" href="{{ route('group.show', [$group->id]) }}">@lang('group.menu_overview')</a>
        <a class="nav-link {{ request()->routeIs('group.character.*') ? 'active' : '' }}" href="{{ route('group.characters', [$group->id]) }}">
          @lang('group.menu_characters')
          <small class="text-muted">({{ $activeCharacterCount }})</small>
        </a>
        <a class="nav-link {{ request()->routeIs('group.user*') && !request()->routeIs('group.character*') ? 'active' : '' }}" href="{{ route('group.user.index', [$group->id]) }}">
          @lang('group.menu_users')
          <small class="text-muted">({{sizeof($group->users)}})</small>
        </a>
        @if( $userIsInCurrentGroup )
          <a class="nav-link ml-auto btn btn-outline-primary btn-sm {{ request()->routeIs('group.character*') ? 'active' : '' }}" href="{{ route('group.character', [$group, auth()->user()]) }}"><i class="fas fa-user"></i> @lang('group.menu_my_character')</a>
        @endif
        @if( auth()->user() -> isGroupAdmin($group->id) )
          <a href="{{ route('group.edit', [$group -> id]) }}#general" class="nav-link btn btn-outline-primary btn-sm {{ request()->routeIs('group.edit') ? 'active' : '' }} ml-3"><i class="fas fa-pencil-alt"></i> @lang('group.menu_group_settings')</a>
        @endif
      </nav>
    </div>
  </div>
  <div class="content container pt-4 bg-light">
    @yield('content')
  </div>
@else
    <div class="container content mt-5">
      <div class="row justify-content-md-center">

        <div class="col-md-6">
          <div class="card">
            <div class="card-body d-flex flex-column">
              <div class="d-flex align-items-top">
                <i class="fas fa-shield-alt h1 text-muted pt-2"></i>
                <div class="pl-3">
                  <h2 class="h4 mb-1">Dies ist eine private Gruppe</h2>
                  <p>{!! __('app.notifications.pending_invite_this_group', ['name' => htmlspecialchars($pendingInvitation -> inviter -> name)]) !!}</p>
                  <div class="text-left">
                    <a class="btn btn-success btn-sm" href="{{route('group.invitation.accept', [$group, $pendingInvitation])}}">@lang('app.accept')</a>
                    <a class="btn btn-danger btn-sm" href="{{route('group.invitation.reject', [$group, $pendingInvitation])}}">@lang('app.decline')</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
@endif
@endsection