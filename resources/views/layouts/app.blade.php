<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="has-navbar-fixed-top">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ __('app.app_title') }}@yield('titleAddition')</title>

  <link rel="shortcut icon"  href="/favicon.ico?ver=2.0" type="image/x-icon"/>
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet">

  <!-- Styles -->
  @yield('head')
</head>
<body>
  @yield('main')
</body>
</html>
