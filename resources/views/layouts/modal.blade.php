<div class="modal" id="{{$id}}">
  <div class="modal-background" onClick="toggleModal('{{$id}}')"></div>
  <div class="modal-content">
    <div class="box">
      <h3 class="title is-3">{{ $title }}</h3>
      <div class="content">
        {{ $slot }}
      </div>
    </div>
  </div>
  <button class="modal-close is-large" onClick="toggleModal('{{$id}}')" aria-label="close"></button>
</div>