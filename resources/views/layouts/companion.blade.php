@extends('layouts.app')
@section('head')
  <link href="{{ asset('css/companion.css') }}" rel="stylesheet">
@endsection
@section('main')
  <nav class="navbar navbar-light navbar-expand-lg">
    <div class="container">

      <div>
        <a class="navbar-brand brand-font mr-4" href="{{route('home')}}">table<span class="text-dark">gamers</span></a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link {{ request()->routeIs('home') ? 'active' : '' }}" href="{{route('home')}}">@lang('app.main_nav.dashboard')</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="my-groups-dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              @lang('app.main_nav.my_groups')
            </a>

            <div class="dropdown-menu" aria-labelledby="my-groups-dropdown">

              @if( $mainNavGroupsAsAdmin -> isEmpty() && $mainNavGroupsAsPlayer -> isEmpty() )
                <h6 class="dropdown-header">@lang('app.main_nav.no_groups_yet')</h6>
              @else

                @if( $mainNavGroupsAsAdmin -> isNotEmpty() )
                  <h6 class="dropdown-header">@lang('app.main_nav.groups_as_admin')</h6>
                  @foreach( $mainNavGroupsAsAdmin as $group )
                    <a class="dropdown-item" href="{{route('group.show', [$group])}}">{{$group -> name}}</a>
                  @endforeach
                @endif

                @if( $mainNavGroupsAsAdmin -> isNotEmpty() && $mainNavGroupsAsPlayer -> isNotEmpty() )
                  <div class="dropdown-divider"></div>
                @endif

                @if( $mainNavGroupsAsPlayer -> isNotEmpty() )
                  <h6 class="dropdown-header">@lang('app.main_nav.groups_as_player')</h6>
                  @foreach( $mainNavGroupsAsPlayer as $group )
                    <a class="dropdown-item" href="{{route('group.show', [$group])}}">{{$group -> name}}</a>
                  @endforeach
                @endif
              @endif
            </div>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item dropdown pr-3">
            <a href="" class="nav-link dropdown-toggle notification" data-toggle="dropdown" href="#" id="alertsDropdown" role="button" aria-expanded="false">
              <i class="align-middle far fa-bell"></i>
              @if( count( Auth::user() -> invitations) > 0 )
                <span class="badge">{{ count( Auth::user() -> invitations ) }}</span>
              @endif
            </a>

            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right py-0" aria-labelledby="alertsDropdown">
              <div class="list-group">
                @if( count( Auth::user() -> invitations ) > 0 )
                  @foreach( Auth::user() -> invitations as $invitation )
                    <div class="list-group-item">
                      <div class="row no-gutters align-items-center">
                        <div class="col-2">
                          <i class="fas fa-exclamation text-light"></i>
                        </div>
                        <div class="col-10">
                          <div class="text-dark">Group Invitation</div>
                          <p class="text-muted small mt-1">
                            {!! __('app.notifications.pending_invite', ['name' => htmlspecialchars($invitation->group->name)]) !!}
                          </p>
                          <a class="btn btn-outline-primary btn-sm" href="{{route('group.show', [$invitation->group_id])}}">@lang('app.notifications.pending_invite_go_to_group')</a>
                          <!-- <div class="text-muted small mt-1">2h ago</div> -->
                        </div>
                      </div>
                    </div>
                  @endforeach
                @else
                  <div class="list-group-item text-muted">
                    @lang('app.notifications.no_notifications')
                  </div>
                @endif
              </div>
              {{--
              <div class="list-group-item dropdown-menu-footer">
                <a href="#">@lang('app.notifications.all_notifications')</a>
              </div>
              --}}
            </div>

          </li>

          <li class="nav-item dropdown user-dropdown border border-top-0 border-right-0 border-bottom-0">
            <a class="nav-link dropdown-toggle btn-sm" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="d-flex align-items-center">
                <span class="user-avatar user-avatar-md">
                  <img src="{{auth()->user()->image}}" alt="">
                </span>
                <span>
                  {{ Auth::user() -> name }}
                </span>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{route('user.show', [auth()->user()])}}">@lang('user.settings')</a>
              <a class="dropdown-item" href="{{route('logout')}}">@lang('user.logout')</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  @if( Auth::check() )
    {{-- Breadcrumbs::render() --}}
  @endif
  
  @yield('body')

  <footer class="footer p-4 pt-4 mt-4 bg-white flex-shrink-0 border border-right-0 border-left-0 border-bottom-0">
    <nav class="nav">
      <div class="container text-center">
        <div class="p-2 bd-highlight">
          <a title="@lang('app.privacy_policy')" class="p-2" href="{{ route('privacy-policy') }}">@lang('app.privacy_policy')</a>
          <small class="text-muted">-</small>
          <a title="@lang('app.imprint')" class="p-2" href="{{ route('imprint') }}">@lang('app.imprint')</a>
        </div>
        <div>
          <a title="Twitter" class="p-1" href="#"><i class="fab fa-twitter-square"></i></a>
          <a title="Facebook" class="p-1" href="#"><i class="fab fa-facebook-square"></i></a>
          <a title="Instagram" class="p-1" href="#"><i class="fab fa-instagram"></i></a>
        </div>
      </div>
    </nav>
  </footer>
  <script src="{{ mix('/js/main.js') }}"></script>
  <script src="/js/all.js"></script>
  @yield('scripts')
@endsection