@extends('layouts.companion')
@section('body')
	@parent
  <div class="container content pt-4">
    <h1 class="h2 mb-2">@yield('title')</h1>
    <div class="card">
      <div class="card-body">
        @yield('content')
      </div>
    </div>
	</div>
@endsection