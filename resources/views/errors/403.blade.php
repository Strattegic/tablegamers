@extends('layouts.errors')
@section('title')
  @lang('public.errors.403.title')
@endsection

@section('message')
  @lang('public.errors.403.message', ['link' => route('home')])
@endsection