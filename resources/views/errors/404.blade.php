@extends('layouts.errors')
@section('title')
  @lang('public.errors.404.title')
@endsection

@section('message')
  @lang('public.errors.404.message', ['link' => route('home')])
@endsection