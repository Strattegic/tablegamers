@extends('layouts.companion_general')
@section('content')
  <h3>User Profile</h3>
  <dl>
    <dt>Username</dt>
    <dd>{{$user->name}}</dd>

    <dt>E-Mail</dt>
    <dd>{{$user->email}}</dd>
  </dl>
@endsection