@extends('layouts.companion_general_empty')
@section('content')

  @if(session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div>
  @endif  

  @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
  @endforeach

  <div class="card">
    <div class="card-body">
      <h2 class="h4">@lang('user.settings')</h2>

      <div class="form-group">
        <label for="inputEmail">@lang('user.email')</label>
        <input type="email" class="form-control" id="inputEmail" disabled value="{{$user->email}}">
      </div>

      <form action="{{route('user.update', [$user])}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
          <label for="inputUsername">@lang('user.username')</label>
          <input type="text" name="name" class="form-control" id="inputUsername" aria-describedby="usernameHelp" value="{{$user->name}}">
          <small id="usernameHelp" class="form-text text-muted">@lang('user.name_help_text')</small>
        </div>

        @include('shared.errors', ['name' => 'settings'])
        <button type="submit" class="btn btn-primary">Änderungen speichern</button>
      </form>
    </div>
  </div>

  <div class="card mt-4">
    <div class="card-body">
      <h2 class="h4">@lang('user.reset_password')</h2>

      <form method="POST" action="{{ route('user.change_password', [$user]) }}">
        @csrf
        @include('shared.errors', ['changePassword'])

        <div class="form-group">
          <input type="password" name="password" class="form-control" id="inputPassword" placeholder="@lang('user.current_password')">
        </div>

        <div class="form-group mt-2">
          <label for="inputNewPassword">@lang('user.new_password')</label>
          <input type="password" name="new_password" class="form-control" id="inputNewPassword" placeholder="@lang('user.new_password')">
        </div>
        <div class="form-group">
          <input type="password" name="new_password_confirmation" class="form-control" id="inputNewPasswordConfirmation" placeholder="@lang('user.new_password_confirmation')">
        </div>

        <button class="btn btn-primary" type="submit">@lang('public.forgot_password_send_request')</button>
      </form>
    </div>
  </div>
  
@endsection