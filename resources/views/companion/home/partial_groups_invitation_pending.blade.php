<div class="card mb-3">
  <div class="card-body">
    <div class="row align-items-center">
      <div class="col-auto">
        
        <!-- Avatar -->
        <a href="{{route('group.show', [$invitation -> group_id])}}" class="avatar avatar-lg">
          <img src="{{ $invitation-> group -> image }}" class="avatar-img">
        </a>

      </div>
      <div class="col">
        
        <!-- Title -->
        <h4 class="card-title mb-1">
          <a href="{{route('group.show', [$invitation -> group_id])}}">{{$invitation-> group -> name}}</a>
        </h4>

        <!-- Status -->
        <p class="card-text small">
          @lang('group.game_system_short'): {{ $invitation-> group -> game_system }}
        </p>

      </div>
      <div class="col-auto">
        
        <!-- Button -->
        <a class="btn btn-outline-success btn-sm" href="{{route('group.invitation.accept', [$invitation-> group, $invitation])}}"><i class="fas fa-check"></i> @lang('app.accept')</a>
        <a class="btn btn-outline-danger btn-sm" href="{{route('group.invitation.reject', [$invitation-> group, $invitation])}}"><i class="fas fa-times"></i> @lang('app.decline')</a>

      </div>
    </div>
  </div>
</div>