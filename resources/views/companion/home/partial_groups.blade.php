<div class="card mb-3">
  <div class="card-body">
    <div class="row align-items-center">
      <div class="col-auto">
        
        <!-- Avatar -->
        <a href="{{route('group.show', [$group])}}" class="avatar avatar-lg">
          <img src="{{ $group -> image }}" class="avatar-img">
        </a>

      </div>
      <div class="col">
        
        <!-- Title -->
        <h4 class="card-title mb-1">
          <a href="{{route('group.show', [$group])}}">{{$group->name}}</a>
        </h4>

        <!-- Status -->
        <p class="card-text small">
          @lang('group.game_system_short'): {{ $group -> game_system }}<br>
          @lang('group.users'): {{$group->users->count()}}
        </p>

      </div>
      <div class="col-auto">
        
        {{--
        <!-- Button -->
        <a href="#!" class="btn btn btn-outline-info d-none d-md-inline-block disabled">
          <i class="fas fa-paper-plane"></i>
        </a>
        --}}

      </div>
    </div>
  </div>
</div>