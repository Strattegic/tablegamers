@extends('layouts.companion')
@section('content')
  <h3 class="title">General</h3>
  <div class="columns">
    <div class="column is-half">
      <form action="{{ route('user_settings.update') }}" method="POST">
        @csrf
        @method('PUT')
        <div class="field">
          <label class="label">Username</label>
          <input type="text" class="input" name="name" value="{{ old('name') ?? $user -> name }}">
        </div>
        <button class="button is-info" type="submit">update</button>
      </form>
    </div>
  </div>
@endsection