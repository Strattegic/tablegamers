<div class="card mb-4">
  <div class="card-body">
    <h2 class="h5">@lang('character.character_files')</h2>
    <p class="text-muted">
      Hier kannst du alle sonstigen Dateien zu deinem Charakter hinzufügen.
    </p>
    
    <div class="row">


      @foreach($files as $file)
        <div class="col-md-6">
          @include('shared.file', ['fileName' => $file -> name, 
          'fileCreatedAt' => $file -> created_at, 
          'fileShowLink' => route('group.character.file.show', [$group, $currentCharacter, $file]), 
          'fileDownloadLink' => route('group.character.file.download', [$group, $currentCharacter, $file]), 
          'fileDeleteLink' => route('group.character.file.destroy', [$group, $currentCharacter, $file]) ])
        </div>
      @endforeach
    </div>

    <form action="{{ route('group.character.file.store', [$group, $currentCharacter]) }}" class="mt-2" method="POST" id="fileUploadForm" enctype="multipart/form-data">
      @csrf

      <div class="form-row">
        <div class="form-group col">
          <input type="text" name="name" value="{{old('name')}}" class="form-control form-control-sm" placeholder="Name">
        </div>
        <div class="form-group col">
          <input type="file" name="file" class="form-control-file form-control-sm">
        </div>
      </div>

      @include('shared.errors')

      <button type="submit" class="btn btn-primary">{{ __('character.character_add_file') }}</button>
    </form>
  </div>
</div>