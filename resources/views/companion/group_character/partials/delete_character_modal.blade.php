@component('shared.modal', ['id' => 'character-destroy-'.$character->id, 'formUrl' => route('group.character.destroy', [$group, $character]), 'formMethod' => 'DELETE', 'confirmation' => __('character.delete_confirmation_button'), 'title' => __('character.delete_confirmation_title')])
      @lang('character.delete_confirmation', ['name' => $character->name])
@endcomponent