@extends('layouts.companion_group')
@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('group.characters', [$group])}}">@lang('group.characters')</a></li>
    <li class="breadcrumb-item active">{{ $character -> name }}</li>
  </ol>
</nav>

<div class="row">
  <div class="col-md-6">
    <div class="card mb-4">
      <div class="card-body">
        <div class="character-info">
          <h2 class="h5">@lang('character.character_sheet')</h2>
          
          @if($character -> characterSheetFiles -> isEmpty())
            <small class="text-muted">@lang('character.character_sheet_none_uploaded')</small>
          @else
            @foreach($character->characterSheetFiles as $file)
              @include('shared.file')
            @endforeach
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card mb-4">
      <div class="card-body character-info">
        <div class="row">
          <div class="col-md-6 d-flex align-items-center">
            <div class="character-avatar mr-4">
              <img src="{{$character->image}}">
            </div>
            <div class="character-attribute">
              <label>@lang('character.name')</label>
              <div>{{ $character -> name }}</div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="character-attribute">
              <label>@lang('character.user_name')</label>
              <small><a href="{{route('group.user.show', [$group, $character -> user])}}">{{ $character -> user -> name }}</a></small>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card">
      <div class="card-body character-info">
        <div class="character-attribute">
          <label>@lang('character.description_short')</label>
          @if(!empty($character -> description))
            <div>{{ $character -> description }}</div>
          @else
            <div>@lang('character.description_none_set')</div>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection