<div class="row">
  @foreach($characters as $character)
    <div class="col-md-3">
      <div class="card text-center mb-4">
        @if( isset( $currentCharacter ) && $currentCharacter -> id == $character -> id )
          <div class="card-body border border-primary rounded">
        @else
          <div class="card-body">
        @endif
          <a href="{{route('group.character.show', [$group, $character])}}">
            <div class="position-relative avatar-u-lg mx-auto mb-3">
              <img class="img-fluid rounded-circle border" src="{{ $character -> image }}" alt="Image Description">
              <span class="badge"><i class="fas fa-star"></i></span>
            </div>
          </a>
          <h2 class="h6 mb-0">
            <a href="{{route('group.character.show', [$group, $character])}}">{{ $character -> name }}</a>
          </h2>
        </div>
        <div class="card-footer">
          <a href="{{route('group.character.edit', [$group, $character])}}" class="btn btn-outline-info btn-sm mr-2"><i class="fas fa-pencil-alt"></i> edit</a>
          <button data-toggle="modal" data-target="#character-destroy-{{$character->id}}" class="btn btn-outline-danger btn-sm"><i class="fas fa-times"></i> delete</button>
        </div>
      </div>
    </div>
    @can('delete', [$character])
      @component('shared.modal.modal_confirmation', ['id' => 'character-destroy-'.$character->id, 'url' => route('group.character.destroy', [$group, $character]), 'method' => 'DELETE'])
        @lang('character.delete_confirmation', ['name' => $character -> name])
      @endcomponent
    @endcan
  @endforeach
</div>
