<div class="row">
  @foreach($characters as $character)
    <div class="col-md-3">
      <div class="card text-center mb-4">
        @if( isset( $currentCharacter ) && $currentCharacter -> id == $character -> id )
          <div class="card-body border border-primary rounded">
        @else
          <div class="card-body">
        @endif
          <a href="{{route('group.character', [$group, $character->user])}}">
            <div class="position-relative avatar-u-lg mx-auto mb-3">
              <img class="img-fluid rounded-circle border" src="{{ $character -> image }}" alt="Image Description">
            </div>
          </a>
          <h2 class="h6 mb-0">
            <a href="{{route('group.character', [$group, $character->user])}}">{{ $character -> name }}</a>
          </h2>
          <a class="small" href="{{route('group.user.show', [$group, $character->user])}}">{{ $character -> user -> name }}</a>
        </div>
      </div>
    </div>
  @endforeach
</div>
