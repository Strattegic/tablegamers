@extends('layouts.companion_group')

@section('content')
  <div class="card">
    <div class="card-body">
      <h3>{{ __('character.create_character') }}</h3>
      @include('shared.errors')
      <form action="{{ route('group.character.store', [$group]) }}" method="POST" enctype="multipart/form-data">
        @csrf

        <div class="row">
          <div class="col-md-7">
            <div class="form-group">
              <label>@lang('character.name')</label>
              <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="@lang('character.name')">
            </div>
            
            <div class="form-row">
              <div class="form-group col-md-12">
                <label>@lang('character.gender')</label>
                <input type="text" class="form-control" name="gender" value="{{ old('gender') }}" placeholder="@lang('character.gender')">
              </div>
              <div class="form-group col-md-12">
                <label>@lang('character.age')</label>
                <input type="text" class="form-control" name="age" value="{{ old('age') }}" placeholder="@lang('character.age')">
              </div>
            </div>

            <!--
            <div class="form-group">
              <label>@lang('character.class')</label>
              <input type="text" class="form-control" name="classname" value="{{ old('classname') }}" placeholder="@lang('character.class')">
            </div>
            <div class="form-group">
              <label>@lang('character.race')</label>
              <input type="text" class="form-control" name="race" value="{{ old('race') }}" placeholder="@lang('character.race')">
            </div>
            -->

          </div>
          <div class="col-md-5">
            <div class="mb-3">
              <!-- <label>@lang('character.image')</label> -->
              <div class="preview-image" id="characterImagePreview" style="background-image: url('/img/character_placeholder.jpg'); min-height: 14em"></div>
              <div class="custom-file mt-2">
                <input type="file" class="custom-file-input" onChange="updatePreviewBackground(this, 'characterImagePreview')" id="validatedCustomFile" name="image">
                <label class="custom-file-label" for="validatedCustomFile">@lang('group.image_please_select')</label>
                <div class="invalid-feedback">Example invalid custom file feedback</div>
                <small class="form-text text-muted">@lang('group.image_help_text')</small>
              </div>
            </div>
          </div>
        </div>

        <!--
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>@lang('character.description')</label>
              <textarea class="form-control" name="description" rows="10" placeholder="@lang('character.description')">{{ old('description') }}</textarea>
            </div>
          </div>
        </div>
        -->

        <button type="submit" class="btn btn-primary">{{ __('character.create') }}</button>
      </form>
    </div>
  </div>
  
@endsection