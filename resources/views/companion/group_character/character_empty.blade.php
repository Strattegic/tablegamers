@extends('layouts.companion_group')
@section('content')

  <div class="card">
    <div class="card-body">

      <h2 class="h5">
        @if( Auth::id() == $user->id )
          @lang('group.menu_my_characters')
        @else
          {{$user->name}}
        @endif
      </h2>

      <em class="text-muted">{!! __('character.no_character_yet_text') !!}</em>
      <a class="btn btn-outline-info mt-2" href="{{route('group.character.create', [$group])}}">
        <i class="fas fa-user-plus"></i> @lang('character.create')
      </a>
    </div>
  </div>
  
@endsection