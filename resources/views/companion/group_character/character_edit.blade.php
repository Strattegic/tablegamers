@extends('layouts.companion_group')
@section('content')
  <div class="card mb-4">
    <div class="card-body">
      <div class="row">

        <div class="col-md-6 mb-4">
          <h2 class="h5">@lang('character.your_character_sheet')</h2>
          <!-- <h3 class="h6 subtitle text-muted">Informationen zu deinem Charakter</h3> -->

          @if(!$character -> characterSheetFiles -> isEmpty())
            @foreach($character -> characterSheetFiles as $file)
              @include('shared.file_default', ['file' => $file, 
              'defaultName' => __('character.character_sheet'),
              'fileDownloadLink' => route('files.download', [$file]), 
              'fileDeleteLink' => route('files.destroy', [$file])
              ])
            @endforeach
          @else
            <div class="alert alert-warning">Du hast noch keinen Charakterbogen hochgeladen.</div>
          @endif
          <form action="{{ route('group.character.file.store', [$group, $character]) }}" method="POST" enctype="multipart/form-data" id="character-sheet-form">
            @csrf
            <input type="hidden" name="file_type" value="character_sheet">
            
            <h6>Datei hinzufügen</h6>
            <div class="form-group">
              <input type="text" class="form-control form-control-sm" name="name" value="{{ old('sheet-file-name') }}" placeholder="@lang('character.name')">
            </div>
            <div class="form-group">
              <input type="file" name="file">
            </div>

            <button class="btn btn-primary">Hochladen</button>
            
          </form>

          @include('shared.errors')
        </div>
        <div class="col-md-6 mb-4">
          <i class="far fa-question-circle"></i>

          <p><small>@lang('character.your_character_sheet_help_text')</small></p>

          Vorlage:<br>
          @if($group -> system -> customFile)
            <p>@lang('group.character_sheet_text_1')</p>
            <a href="{{route('files.download', [$group -> system -> customFile])}}"><i class="fas fa-download"></i> @lang('group.character_sheet')</a>
          @else
            <p><small><em>@lang('character.character_sheet_text_1_no_sheet')</em></small></p>
          @endif
        </div>
          


      </div>
    </div>
  </div>

  <div class="card mb-4">
    <div class="card-body">
      <h2 class="h5">Charakterinformationen für deine Gruppe</h2>
      <!-- <h3 class="subtitle h6 mb-3 text-muted">Informationen für deine Gruppe</h3> -->
      
      <form action="{{ route('group.character.update', [$group, $character]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="form-group">
          <div class="media">
            <div class="fileinput-button character-avatar">
              <img src="{{$character->image}}" id="characterImagePreview" class="preview-image">
              <input type="file" class="custom-file-input" onChange="updatePreviewImage(this, 'characterImagePreview')" id="validatedCustomFile" name="image">
            </div>
            <div class="media-body pl-3">
              <h3 class="media-title">Dein Charakterbild</h3>
              <h6 class="media-subtitle text-muted">Klicke auf den Avatar um das Bild zu ändern.</h6>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label>@lang('character.name')</label>
          <input type="text" class="form-control" name="name" value="{{ $character -> name or old('name') }}" placeholder="@lang('character.name')">
        </div>

          <div class="form-group">
            <!--
            <div class="custom-file my-2">
              <input type="file" class="custom-file-input" onChange="updatePreviewImage(this, 'characterImagePreview')" id="validatedCustomFile" name="image">
              <label class="custom-file-label" for="validatedCustomFile">@lang('group.image_please_select')</label>
              <div class="invalid-feedback">Example invalid custom file feedback</div>
              <small class="form-text text-muted">@lang('group.image_help_text')</small>
            </div>
            -->
          </div>

          <div class="form-group">
            <label>@lang('character.description_short')</label>
            <textarea class="form-control" name="description" rows="10" placeholder="@lang('character.description_short')">{{ $character -> description or old('description') }}</textarea>
          </div>

        @include('shared.errors')

        <button type="submit" class="btn btn-primary">{{ __('character.save_changes') }}</button>
      </form>
    </div>
  </div>
@endsection