@extends('layouts.companion_general')
@section('content')

  <h1 class="h3">@lang('group.create_new_group')</h1>

  <form method="POST" action="{{ route('group.store') }}" enctype="multipart/form-data">
    @csrf

    <p class="form-text">
      Um eine neue Gruppe zu erstellen, fülle einfach die unten angezeigten Felder aus. <br>
      Nachdem die Gruppe erfolgreich erstellt wurde kannst du das System wählen, Spieler hinzufügen und mehr Informationen angeben.
    </p>

    <div class="form-group">
      <label for="inputName">Name</label>
      <input type="text" name="name" class="form-control" value="{{ $group -> name or old('name') }}" id="inputName" placeholder="Name">
    </div>
    {{--
    <div class="form-group">
      <label for="inputName">@lang('group.game_system')</label>
      <select name="game_system_id" id="game_system_select" class="form-control" onChange="changedGameSystem(this, 'game-system-name')">
        @foreach($gameSystems as $gameSystem)
          <option value="{{$gameSystem -> id}}" {{old('game_system_id') == $gameSystem -> id ? "selected": ""}}>{{__($gameSystem -> name)}}</option>
        @endforeach
      </select>
      <small class="form-text text-muted"><i class="fas fa-exclamation"></i> Dieses System ermöglicht es dir einen eigenen Charakterbogen hochzuladen und diesen von deinen Spielern ausfüllen zu lassen.</small>
      <input type="text" name="game_system_name" class="form-control mt-2" value="{{ $group -> game_system_name or old('game_system_name') }}" id="inputGameSystem" placeholder="@lang('app.game_system.default_name')">
    </div>--}}
    <div class="form-group">
      <label for="inputDescription">Description</label>
      <textarea class="form-control" name="description" id="inputDescription" rows="3">{{ $group -> description or old('description') }}</textarea>
    </div>

    @include('shared.errors')

    <button type="submit" class="btn btn-primary float-left">{{ __('group.create') }}</button>
  </form>

  <script type="text/javascript">
    window.onload = function(){
      changedGameSystem(document.getElementById('game_system_select'), 'game-system-name');
    }
  </script>

@endsection