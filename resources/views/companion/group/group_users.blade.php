@extends('layouts.companion_group')
@section('content')

  @if($isGroupAdmin)
  <div class="card mb-4">
    <div class="card-body">
      <h2 class="h5">@lang('group.invite_user.invite_text')</h2>

      @include('shared.errors')

        <form action="{{route('group.invite_user', [$group->id])}}#group" method="POST" class="mt-3">
          @csrf

          <div class="row align-items-center">
            <div class="col">
              <input type="email" name="email" value="{{ old('email') }}" class="form-control form-control-sm" placeholder="@lang('group.invite_user.email')">
            </div>
            <div class="col">
              <input type="text" name="name" value="{{ old('name') }}" class="form-control form-control-sm" placeholder="@lang('group.invite_user.player_name')">
            </div>
            <div class="col-auto">
              <button type="submit" class="btn btn-outline-primary btn-sm">@lang('group.invite_user.invite_text_short')</button>
            </div>
          </div>

        </form>

    </div>
  </div>
  @endif

  @foreach( $users as $user )
  <div class="card mb-3 {{auth()->id()==$user->id ? 'border border-primary' : ''}}">
    <div class="card-body">
      <div class="row align-items-center">
        <div class="col-auto">
          
          <!-- Avatar -->
          <a class="avatar avatar-lg">
            <img src="{{ $user -> image }}" class="avatar-img rounded-circle">
          </a>

        </div>
        <div class="col">
          
          <!-- Title -->
          <h4 class="card-title mb-1">
            <a>
              {{$user->name}}
            </a>
          </h4>

          <!-- Status -->
          @if( $user -> isGroupAdmin($group->id) )
          <p class="card-text small">
            @lang('group.admin')
          </p>
          @endif

        </div>
        <div class="col-auto">
          
          <!-- Button 
          <a href="#!" class="btn btn btn-outline-info d-none d-md-inline-block disabled">
            <i class="fas fa-paper-plane"></i>
          </a>
          -->
          
          @if( $user -> is_deletable )
            <button data-toggle="modal" data-target="#group-user-destroy-{{$user->id}}" class="btn btn btn-outline-danger d-none d-md-inline-block">
              <i class="fas fa-user-minus"></i>
            </button>
          @endif

        </div>
      </div>
    </div>
  </div>
    @component('shared.modal.modal_confirmation', ['id' => 'group-user-destroy-'.$user->id, 'url' => route('group.user.destroy', [$group, $user]), 'method' => 'DELETE'])
      @lang('group.user_delete_confirmation', ['name' => $user -> name])
    @endcomponent
  @endforeach

  @if($group->invitations->isNotEmpty())

    <hr class="my-4">
    @foreach($group->invitations as $invitation)

      <div class="card mb-3">
        <div class="card-body">
          <div class="row align-items-center">
            <div class="col-auto">
              
              <!-- Avatar -->
              <a class="avatar avatar-lg">
                <img src="{{asset( config('tablegamers.user_placeholder_image') )}}" alt="..." class="avatar-img rounded-circle">
              </a>

            </div>
            <div class="col">
              
              <!-- Title -->
              <h4 class="card-title mb-1">
                {{$invitation->name}}
              </h4>

              <!-- Text -->
              <p class="card-text small text-muted mb-1">
                invited {{$invitation->updated_at->diffForHumans()}}
              </p>

            </div>
            <div class="col-auto">

              @if($isGroupAdmin)
              <button data-toggle="modal" data-target="#group-invitation-destroy-{{$invitation->id}}" class="btn btn btn-outline-danger d-none d-md-inline-block">
                <i class="fas fa-user-minus"></i>
              </button>
              @endif

            </div>
          </div>
        </div>
      </div>
      @component('shared.modal.modal_confirmation', ['id' => 'group-invitation-destroy-'.$invitation->id, 'url' => route('group.invitation.destroy', [$group, $invitation]), 'method' => 'DELETE'])
      @lang('group.user_invitation_confirmation', ['name' => $invitation -> name])
    @endcomponent
    @endforeach
  @endif

@endsection