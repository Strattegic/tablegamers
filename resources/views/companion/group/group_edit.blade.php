@extends('layouts.companion_group')
@section('content')

  @include('shared.errors')

  <div id="general" class="card mb-4">
    <div class="card-body">
      <!-- <h2 class="h5">@lang('group.menu_group_settings')</h2> -->
      <form method="POST" action="{{ route('group.update', [$group->id]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="row">
          <div class="col-md-6">
            
            <div class="form-group">
              <label for="inputName" class="h5">@lang('group.name')</label>
              <input type="text" name="name" class="form-control" value="{{ old('name', $group -> name) }}" id="inputName" placeholder="Name">
            </div>

            <div class="form-group">
              <label for="inputDescription" class="h5">@lang('group.visibility')</label>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="is_private" id="visibilityRadioPrivate" value="1" checked>
                <label class="form-check-label" for="visibilityRadioPrivate">
                  @lang('group.visibility_status.private')
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="is_private" id="visibilityRadioPublic" value="0" disabled>
                <label class="form-check-label" for="visibilityRadioPublic">
                  @lang('group.visibility_status.public')
                </label>
              </div>
            </div>

            <div class="form-group">
              <label for="inputNextSessionTime" class="h5">@lang('group.next_session')</label>
              <div class="row">
                <div class="form-group col-md-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="far fa-clock"></i></div>
                    </div>
                    <input type="datetime-local" name="next_session_datetime" class="form-control" value="{{ isset( $group -> next_session_datetime ) ? old('next_session_datetime', $group -> next_session_datetime -> format('Y-m-d\TH:i')) : old('next_session_datetime') }}" id="inputNextSessionTime" placeholder="@lang('group.next_session_time')">
                  </div>
                </div>
                <div class="form-group col-md-12">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text"><i class="far fa-compass"></i></div>
                    </div>
                    <input type="text" name="next_session_location" class="form-control" value="{{ old('next_session_location', $group -> next_session_location) }}" id="inputNextSessionLocation" placeholder="@lang('group.next_session_location')">
                  </div>
                </div>
              </div>
            </div>
            
          </div>

          <div class="col-md-6">

            <div class="form-group">
              <label class="h5">@lang('group.image')</label>
              <!-- <div class="preview-image" style="background-image: url('{{ $group -> image }}');"></div> -->
              <img class="preview-image mb-2 d-block" id="groupImagePreview" src="{{ $group -> image }}">
              <div class="custom-file">
                <input type="file" class="custom-file-input" onChange="updatePreview(this, 'groupImagePreview')" id="validatedCustomFile" name="image">
                <label class="custom-file-label" for="validatedCustomFile">@lang('group.image_please_select')</label>
                <small class="form-text text-muted">@lang('group.image_help_text')</small>
              </div>
            </div>

          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label for="inputDescription" class="h5">@lang('group.description')</label>
              <textarea class="form-control" name="description" id="inputDescription" rows="8">{{old('description', $group -> description)}}</textarea>
            </div>
          </div>

        </div>
        <!-- <div class="row mt-3">
          <div class="col-md-12">
            <label for="inputDescription">@lang('group.description')</label>
            <textarea class="form-control" name="description" id="inputDescription" rows="8">{{ $group -> description or old('description') }}</textarea>
          </div>
        </div> -->
        <button type="submit" class="btn btn-primary">@lang('group.save_changes')</button>
      </form>
    </div>
  </div>

  <div id="system" class="card">
    <div class="card-body">
      <h2 class="h5">@lang('group.game_system')</h2>
      <form method="POST" action="{{ route('group.game-system.update', [$group, $group->system]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group mb-0">
          <select name="game_system_id" id="game_system_select" class="form-control" onChange="changedGameSystem(this, 'game-system-wrapper')">
            @foreach($gameSystems as $gameSystem)
              <option value="{{$gameSystem -> id}}" {{old('game_system_id') == $gameSystem -> id ? "selected": ""}}>@lang($gameSystem -> name)</option>
            @endforeach
          </select>
        </div>
        <div id="game-system-wrapper">
          <small class="form-text text-muted mb-2"><i class="fas fa-exclamation"></i> @lang('app.game_system.default.helptext')</small>
          <div class="form-group">
            <label class="mb-0">@lang('group.game_system_name')</label>
            <input type="text" name="custom_name" class="form-control mt-2" value="{{ old('custom_game_system_name', $group -> system -> custom_name) }}" id="inputGameSystem" placeholder="@lang('app.game_system.default.name')">
          </div>
          <div class="form-group">
            <label class="mb-0">@lang('group.character_sheet')</label>
            <small class="form-text text-muted mt-0">@lang('app.game_system.default.character_sheet_helptext')</small>
            <div class="file d-flex align-items-center py-2">
              <div class="file-icon" style="font-size:2em">
                <i class="far fa-file"></i>
              </div>
              <div class="file-info px-2" style="flex: 1 0 0px;">
                @if(!empty($group -> system -> customFile))
                  <a class="file-title" href="{{route('files.download', [$group -> system -> customFile])}}" target="_blank"><i class="fas fa-download"></i> @lang('group.character_sheet')</a>
                  <div class="file-subtitle text-muted">
                    <small> 
                      @if(!empty($group -> system -> customFile->size))
                        {{$group -> system -> customFile -> size}},  
                      @endif
                      @lang('app.file.modified_at', ['time' => $group -> system -> customFile -> created_at -> diffForHumans()])
                    </small>
                  </div>
                @else
                  <span class="text-muted font-italic" id="no-file-uploaded-message">@lang('app.character_sheet_not_uploaded')</span>
                @endif
              </div>
              <div class="file-buttons d-flex">
                <div class="upload-btn-wrapper ml-2">
                  <button class="btn btn-primary btn-sm"><i class="fas fa-upload"></i> @lang('app.upload')</button>
                  <input type="file" name="custom_file" id="input-character-sheet" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <button type="submit" class="btn btn-primary">@lang('group.save_changes')</button>
      </form>
    </div>
  </div>

  <div class="card mt-4">
    <div class="card-body">
      <h2 class="h5">@lang('group.delete_group')</h2>
        <p>@lang('group.delete_group_text')</p>
        <button type="submit" data-toggle="modal" data-target="#group-destroy" class="btn btn-danger">@lang('group.delete')</button>
    </div>
  </div>
  @component('shared.modal.modal_confirmation', ['id' => 'group-destroy', 'url' => route('group.destroy', [$group]), 'method' => 'DELETE'])
    @lang('group.delete_confirmation', ['name' => $group -> name])
  @endcomponent

  <script type="text/javascript">
    document.getElementById('input-character-sheet').onchange = function () {
      var message = document.getElementById('no-file-uploaded-message');
      message.innerHTML = this.value.replace(/.*[\/\\]/, '');
      message.classList.remove('text-muted');
      message.classList.remove('font-italic')
    };
  </script>
@endsection