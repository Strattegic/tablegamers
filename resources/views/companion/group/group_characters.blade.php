@extends('layouts.companion_group')
@section('content')

  @if( sizeof( $characters ) > 0 )

    @include('companion.group_character.card_deck_characters')

  @else
    <div class="card">
      <div class="card-body">
        <em class="text-muted">@lang('character.no_character_yet')</em>
      </div>
    </div>
  @endif
@endsection