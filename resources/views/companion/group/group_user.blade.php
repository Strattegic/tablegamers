@extends('layouts.companion_group')
@section('content')

  <div class="card">
    <div class="card-body">
      <h3>{{ $user -> name }}</h3>
      @if( $user->joined_group_at != null )
        <strong>Gruppe beigetreten:</strong> {{$user->joined_group_at->toFormattedDateString()}} ({{$user->joined_group_at->diffForHumans()}})
      @endif
    </div>
  </div>
  
@endsection