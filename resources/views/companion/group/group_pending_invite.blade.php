@extends('layouts.companion')
@section('content')
  <div class="card">
    <div class="card-body">
      <h2 class="h4">{{$group->name}}</h2>
      <p>{!! __('app.notifications.pending_invite_this_group', ['name' => htmlspecialchars($pendingInvitation -> inviter -> name)]) !!}</p>
      <div class="text-center">
        <a class="btn btn-outline-success btn-sm" href="{{route('group.invitation.accept', [$group, $pendingInvitation])}}">@lang('app.accept')</a>
        <a class="btn btn-outline-danger btn-sm" href="{{route('group.invitation.reject', [$group, $pendingInvitation])}}">@lang('app.decline')</a>
      </div>
    </div>
  </div>
@endsection