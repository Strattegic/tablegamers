@extends('layouts.companion_group')
@section('content')
  
  <div class="row">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <h2 class="h5">@lang('group.overview_title')</h2>
          <div class="text-content">
            @if( $group -> description )
              {{ $group -> description }}
            @else
              <div></div><em class="text-muted">@lang('group.description_none')</em>
            @endif
          </div>
        </div>
      </div>
    </div>
    @if($progress -> messages -> isNotEmpty())
      <div class="col-md-4">
        <div class="card mb-4">
          <div class="card-body">
            @if($isGroupAdmin)
              <h2 class="h5">@lang('group.progress.title_admin') <small><i class="far fa-question-circle" title="@lang('group.progress.subtitle_admin')"></i></small></h2>
            @else
              <h2 class="h5">@lang('group.progress.title_player')</h2>
            @endif
            <div class="group-progress">
              <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: {{$progress->percentage}}%">{{$progress->percentage}}%</div>
              </div>
              <ul class="group-progress-list fa-ul">
                @foreach($progress -> messages as $message)
                <li>
                  @if($message -> isReady)
                    <span class="fa-li text-success"><i class="fas fa-check-square"></i></span>
                  @else
                    <span class="fa-li"><i class="far fa-square"></i></span>
                  @endif
                  {!! $message -> text !!}
                </li>
                @endforeach
              </ul>
            </div>
            {{--
            <div class="text-right">
              <a class="btn btn-outline-info" data-toggle="collapse" href="#collapseExample">Ausblenden</a>
            </div>
            --}}
          </div>
        </div>
      </div>
    @endif
	</div>
@endsection