@extends('layouts.companion')
@section('body')

  @if( !empty( $pendingInvitation ) )
    <div class="container mt-5">
      <div class="row justify-content-md-center">

        <div class="col-md-6">
          <div class="card">
            <div class="card-body">
              <h2 class="h4">{{$group->name}}</h2>
              <p>{!! __('app.notifications.pending_invite_this_group', ['name' => htmlspecialchars($pendingInvitation -> inviter -> name)]) !!}</p>
              <div class="text-center">
                <a class="btn btn-outline-success btn-sm" href="{{route('group.invitation.accept', [$group, $pendingInvitation])}}">@lang('app.accept')</a>
                <a class="btn btn-outline-danger btn-sm" href="{{route('group.invitation.reject', [$group, $pendingInvitation])}}">@lang('app.decline')</a>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  @else
    <div class="container mt-5">
      <div class="row justify-content-md-center">

        <div class="col-md-6">
          <div class="card">
            <div class="card-body text-center">
              <h2><i class="fas fa-ban text-muted"></i></h2>
              <p>@lang('group.private_group.message')</p>
              <a href="{{route('home')}}" class="btn btn-outline-primary btn-sm"><i class="fas fa-home"></i> @lang('group.private_group.back_to_my_groups')</a>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  @endif

@endsection