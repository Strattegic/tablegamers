@extends('layouts.companion_general')
@section('title') 
  @lang('app.your_groups')
@endsection
@section('content')

  <h2 class="h5">@lang('group.create_new_group')</h2>
  
  <form method="POST" action="{{ route('group.store') }}" enctype="multipart/form-data">
    @csrf
    <div class="row align-items-center">
      <div class="col">
        <input type="text" name="name" class="form-control form-control-sm" value="{{ $group -> name or old('name') }}" id="inputName" placeholder="Name">
      </div>
      <div class="col-auto">
        <button class="btn btn-outline-info btn-sm" type="submit" id="button-addon2"><i class="fas fa-users"></i> @lang('group.create_new_group')</button>
      </div>
    </div>
  </form>

  @if($groups->isEmpty())
    <em class="text-muted">@lang('group.no_groups_yet')</em>
  @endif

  <div class="row mt-4">
    @foreach($groups as $group)
    <div class="col-md-4">
      <div class="card text-center mb-4">
        <div class="card-body group-card">
          <a href="{{route('group.show', [$group])}}">
            <div class="position-relative avatar-u-lg mx-auto mb-3">
              <img class="img-fluid border" src="{{ $group -> image }}" alt="Image Description">
            </div>
          </a>
          <h2 class="h6 mb-0 group-name">
            <a href="{{route('group.show', [$group])}}">{{ $group -> name }}</a>
          </h2>
          <a class="small group-system" href="{{route('group.show', [$group])}}">{{ $group -> name }}</a>
          <div class="my-2">
            <div class="group-users">
              @foreach($group -> users as $user)
              <a href="#!" class="group-user user-avatar user-avatar-sm" data-toggle="tooltip" title="" data-original-title="{{$user->name}}">
                <img src="{{$user->image}}" alt="">
              </a>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>

@endsection