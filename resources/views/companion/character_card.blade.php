<div class="column is-3">
  <div class="card">
    <div class="card-image">
      <a href="{{ route_with_identifier('character.edit', ['groupId' => $group -> id, 'playerId' => $player -> id, 'characterId' => $character -> id]) }}">
        <figure class="image is-square character-card-image" style="background-image: url('{{ $character -> image }}');">
          <!--<img src="{{ $character -> display_image or asset('img/character_placeholder.jpg') }}" alt="Placeholder image">-->
        </figure>
      </a>
    </div>
    @if( $character -> name )
    <div class="card-content">
      <div class="media">
        <div class="media-content">
          <a href="{{ route_with_identifier('character.edit', ['groupId' => $group -> id, 'playerId' => $player -> id, 'characterId' => $character -> id]) }}">
            <p class="title is-5">
              {{ $character -> name }}
            </p>
          </a>
          @if( $character -> is_main )
          <p class="subtitle is-6">{{ __('player.character_main') }}</p>
          @endif
        </div>
      </div>
      @if($character -> is_active)
        <span class="tag is-info is-small">{{ __('character.active_character') }}</span>
      @endif
    </div>
    @endif
  </div>
</div>