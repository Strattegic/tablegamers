@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @if(!empty($name))
        @foreach ($errors->getMessages($name) as $error)
          <li>{{ $error }}</li>
        @endforeach
      @else
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      @endif
    </ul>
  </div>
@endif