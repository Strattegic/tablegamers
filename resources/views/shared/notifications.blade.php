@if( count( auth()->user() -> invitations ) > 0 )
  @foreach( auth()->user() -> invitations as $invitation )
    <div class="card">
      <div class="card-body">
        <div class="list-group-item">
          <div class="row no-gutters align-items-center">
            <div class="col-2">
              <i class="fas fa-exclamation text-light"></i>
            </div>
            <div class="col-10">
              <div class="text-dark">Group Invitation</div>
              <p class="text-muted small mt-1">
                {!! __('app.notifications.pending_invite', ['name' => $invitation->group->name]) !!}
              </p>
              <a class="btn btn-outline-primary btn-sm" href="{{route('group.show', [$invitation->group_id])}}">@lang('app.notifications.pending_invite_go_to_group')</a>
              <!-- <div class="text-muted small mt-1">2h ago</div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  @endforeach
@endif