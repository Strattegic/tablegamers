<div class="modal fade" id="{{ $id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form action="{{ $formUrl }}" method="{{ $formMethod }}">
        @csrf
        @method($formMethod)
        <div class="modal-header">
          @if( isset( $title ) )
          <h5 class="modal-title" id="exampleModalLabel">{{ $title }}</h5>
          @endif
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          {{ $slot }}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('app.modal_cancel')</button>
          <button type="submit" class="btn btn-primary">{{ $confirmation }}</button>
        </div>
      </form>
    </div>
  </div>
</div>