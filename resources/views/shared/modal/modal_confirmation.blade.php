@extends('shared.modal.modal_base')
@section('modal-content')
  <form action="{{ $url }}" method="POST">
    @csrf
    @method($method)
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">@lang('app.modal.confirmation_title')</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      {{ $slot }}
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('app.modal.confirmation_cancel_button')</button>

      @if(isset($modalType) && $modalType == 'danger')
      <button type="submit" class="btn btn-danger">{{ $confirmation or __('app.modal.confirmation_button') }}</button>
      @else
      <button type="submit" class="btn btn-primary">{{ $confirmation or __('app.modal.confirmation_button') }}</button>
      @endif
    </div>
  </form>
@overwrite
