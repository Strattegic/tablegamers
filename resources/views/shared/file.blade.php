<div class="file d-flex align-items-center pb-3">
  <div class="file-icon" style="font-size:2em">
    @if(isset($fileType) && $fileType == 'application/pdf')
      <i class="far fa-file-pdf"></i>
    @else
      <i class="far fa-file"></i>
    @endif
  </div>
  <div class="file-info px-2" style="flex: 1 0 0px;">
    @isset($file)
      <a class="file-title" href="{{ route('files.download', [$file]) }}" target="_blank"><i class="fas fa-download"></i> 
        @if(empty($file -> name) && isset($defaultName))
          {{ $defaultName }}
        @else
          {{ $file -> name }}
        @endif
      </a>
      <div class="file-subtitle text-muted">
        <small> 
          @if(!empty($file->size))
            {{$file -> size}},  
          @endif
          @lang('app.file.modified_at', ['time' => $file -> created_at -> diffForHumans()])
        </small>
      </div>
    @else
      <em class="text-muted">@lang('app.file.no_upload_yet')</em>
    @endisset
  </div>
  <div class="file-buttons d-flex">
    @if(isset($fileDownloadLink))
      <a class="btn btn-sm btn-info" href="{{ $fileDownloadLink }}"><i class="fas fa-download text-light"></i></a>
    @endif
    @if(isset($buttons))
      {{ $buttons }}
    @endif
    @if(isset($fileDeleteLink))
    <form action="{{ $fileDeleteLink }}#character" method="POST" style="display: inline;">
      @csrf
      @method('DELETE')
      <button type="submit" class="btn btn-sm btn-danger"><i class="far fa-trash-alt text-light"></i></button>
    </form>
    @endif
  </div>
</div>