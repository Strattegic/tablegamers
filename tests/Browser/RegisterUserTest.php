<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterUserTest extends DuskTestCase
{
    use DatabaseMigrations;


    protected function setUp()
    {
        parent::setUp();
    }

    public function test_errors()
    {
        // test for an error message if not all fields are filled
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->type('register_name', 'Dingenskirchen')
                ->type('register_email', 'someone@outlook.com')
                ->type('register_password', 'secret')
                ->press('Register')
                ->assertSee('field is required')
                ->assertPathIs('//login');
        });
    }

    public function test_register()
    {
        // test a successful registration
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->type('register_name', 'Dingenskirchen')
                ->type('register_email', 'someone@outlook.com')
                ->type('register_password', 'secret')
                ->check('register_data_privacy')
                ->press('Register')
                ->assertPathIs('/companion');
        });
    }

    public function test_register_user_with_already_registered_player()
    {
        $group = factory(\App\Group::class)->create();

        // create a player without user_id (invited by group leader)
        $player = factory(\App\Player::class)->create([
            'group_id' => $group -> id
        ]);


        // register new user with the email address of the created player
        $this->browse(function (Browser $browser) use ($player) {
            $browser->visit('login')
                ->type('register_name', 'Dingenskirchen')
                ->type('register_email', $player -> email)
                ->type('register_password', 'secret')
                ->check('register_data_privacy')
                ->press('Register')
                ->assertPathIs('/companion');
        });

        // Now the system should have recognized that there is already a player
        // with the email address. It should set the user_id on all players with this address.

        $user = \App\User::where('email', $player -> email)->first();

        $this -> assertNotNull( $user );
        $this -> assertTrue( $user -> email == $player -> email );

        // check if the database has actually a player with a reference to the newly created user
        $this -> assertDatabaseHas('players', [
            'id' => $player -> id,
            'user_id' => $user -> id
        ]);
    }
}
