<?php

namespace Tests\Feature\UserAccess;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserProfileAccessTest extends TestCase
{

  protected $followRedirects = true;

  /**
   * A basic test example.
   *
   * @return void
   */
  public function test_authenticated_user_can_update_self()
  {
    $user = factory(User::class)->create();
    $this -> actingAs($user) -> put(route('user.update', [$user])) -> assertStatus(200);
  }

  public function test_user_cannot_change_data_for_other_user()
  {
    $user = factory(User::class)->create();
    $user2 = factory(User::class)->create();
    $this -> actingAs($user) -> put(route('user.update', [$user2])) -> assertStatus(403);
  }

  public function test_user_can_delete_self()
  {
    $user = factory(User::class)->create();
    $this -> actingAs($user) -> delete(route('user.destroy', [$user])) -> assertStatus(200);
  }

  public function test_user_cannot_delete_for_other_user()
  {
    $user = factory(User::class)->create();
    $user2 = factory(User::class)->create();
    $this -> actingAs($user) -> delete(route('user.destroy', [$user2])) -> assertStatus(403);
  }

  public function test_user_can_change_password()
  {
    $user = factory(User::class)->create();
    $this -> actingAs($user) -> post(route('user.change_password', [$user])) -> assertStatus(200);
  }

  public function test_user_cannot_change_password_for_other_user()
  {
    $user = factory(User::class)->create();
    $user2 = factory(User::class)->create();
    $this -> actingAs($user) -> post(route('user.change_password', [$user2])) -> assertStatus(403);
  }
}
