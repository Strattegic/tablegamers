<?php

namespace Tests\Feature\UserAccess;

use App\User;
use App\Character;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CharacterAccessTest extends TestCase
{

  protected $followRedirects = true;


    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_authenticated_user_without_group()
    {
      $group = factory(\App\Group::class)->create();

      $originalUser = factory(User::class)->create();
      $character = factory(Character::class)->create(['user_id' => $originalUser ->id, 'group_id' => $group -> id]);

      $user = factory(User::class)->create();

      $this -> actingAs($user) -> get(route('group.user.character.index', [$group, $originalUser])) -> assertStatus(403);
      $this -> actingAs($user) -> get(route('group.user.character.create', [$group, $originalUser])) -> assertStatus(403);
      $this -> actingAs($user) -> post(route('group.user.character.store', [$group, $originalUser])) -> assertStatus(403);
      $this -> actingAs($user) -> get(route('group.user.character.show', [$group, $originalUser, $character])) -> assertStatus(403);
      $this -> actingAs($user) -> get(route('group.user.character.edit', [$group, $originalUser, $character])) -> assertStatus(403);
      $this -> actingAs($user) -> put(route('group.user.character.update', [$group, $originalUser, $character])) -> assertStatus(403);
      $this -> actingAs($user) -> delete(route('group.user.character.destroy', [$group, $originalUser, $character])) -> assertStatus(403);
    }

    public function test_user_access_to_own_character()
    {
      $group = factory(\App\Group::class)->create();

      $user = factory(User::class)->create();
      $user->groups()->attach($group);
      $character = factory(Character::class)->create(['user_id' => $user ->id, 'group_id' => $group -> id, 'is_active' => true]);

      $this -> actingAs($user) -> get(route('group.user.character.index', [$group, $user])) -> assertStatus(200);
      $this -> actingAs($user) -> get(route('group.user.character.create', [$group, $user])) -> assertStatus(200);
      $this -> actingAs($user) -> followingRedirects() -> post(route('group.user.character.store', [$group, $user])) -> assertStatus(200);
      $this -> actingAs($user) -> get(route('group.user.character.show', [$group, $user, $character])) -> assertStatus(200);
      $this -> actingAs($user) -> get(route('group.user.character.edit', [$group, $user, $character])) -> assertStatus(200);
      $this -> actingAs($user) -> followingRedirects() -> put(route('group.user.character.update', [$group, $user, $character])) -> assertStatus(200);
      $this -> actingAs($user) -> followingRedirects() -> delete(route('group.user.character.destroy', [$group, $user, $character])) -> assertStatus(200);
    }

    public function test_user_access_to_other_character()
    {
      $group = factory(\App\Group::class)->create();

      $originalUser = factory(User::class)->create();
      $originalUser->groups()->attach($group);
      $character = factory(Character::class)->create(['user_id' => $originalUser ->id, 'group_id' => $group -> id, 'is_active' => true]);
      $characterNotActive = factory(Character::class)->create(['user_id' => $originalUser ->id, 'group_id' => $group -> id, 'is_active' => false]);

      $user = factory(User::class)->create();
      $user->groups()->attach($group);

      $this -> actingAs($user) -> get(route('group.user.character.index', [$group, $originalUser])) -> assertStatus(200);
      $this -> actingAs($user) -> get(route('group.user.character.create', [$group, $originalUser])) -> assertStatus(403);
      $this -> actingAs($user) -> followingRedirects() -> post(route('group.user.character.store', [$group, $originalUser])) -> assertStatus(403);
      $this -> actingAs($user) -> get(route('group.user.character.show', [$group, $originalUser, $character])) -> assertStatus(200);
      $this -> actingAs($user) -> get(route('group.user.character.show', [$group, $originalUser, $characterNotActive])) -> assertStatus(403);
      $this -> actingAs($user) -> get(route('group.user.character.edit', [$group, $originalUser, $character])) -> assertStatus(403);
      $this -> actingAs($user) -> followingRedirects() -> put(route('group.user.character.update', [$group, $originalUser, $character])) -> assertStatus(403);
      $this -> actingAs($user) -> followingRedirects() -> delete(route('group.user.character.destroy', [$group, $originalUser, $character])) -> assertStatus(403);
    }
}
