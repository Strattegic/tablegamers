<?php

namespace Tests\Feature\UserAccess;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GroupAccessTest extends TestCase
{

    public function test_kicking_user_from_group()
    {
      $group = factory(\App\Group::class)->create();
      $userNotInGroup = factory(User::class)->create();
      $userInGroup = factory(User::class)->create();
      $userInGroup->groups()->attach($group);

      $adminUser = factory(User::class)->create();
      $adminUser->groups()->attach($group, ['is_admin' => true]);

      $normalUser = factory(User::class)->create();
      $normalUser->groups()->attach($group, ['is_admin' => false]);

      // kicking a normal user in the group
      $this -> actingAs($normalUser) -> followingRedirects() -> delete(route('group.user.destroy', [$group, $userInGroup])) -> assertStatus(403);
      $this -> actingAs($adminUser) -> followingRedirects() -> delete(route('group.user.destroy', [$group, $userInGroup])) -> assertStatus(200);

      // kicking self as admin and normal user
      $this -> actingAs($normalUser) -> followingRedirects() -> delete(route('group.user.destroy', [$group, $normalUser])) -> assertStatus(200);
      $this -> actingAs($adminUser) -> followingRedirects() -> delete(route('group.user.destroy', [$group, $adminUser])) -> assertStatus(200);

      // kicking as a user that is not in the group
      $this -> actingAs($userNotInGroup) -> delete(route('group.user.destroy', [$group, $normalUser])) -> assertStatus(403);
      $this -> actingAs($userNotInGroup) -> delete(route('group.user.destroy', [$group, $adminUser])) -> assertStatus(403);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_access_from_authenticated_user_without_group()
    {
      $group = factory(\App\Group::class)->create();
      $user = factory(User::class)->create();

      $this -> actingAs($user) -> get(route('group.create')) -> assertStatus(200);
      $this -> actingAs($user) ->followingRedirects() -> post(route('group.store'), ['name' => 'ExampleGroup']) -> assertStatus(200);
      $this -> actingAs($user) -> get(route('group.show', [$group])) -> assertStatus(403);
      $this -> actingAs($user) -> get(route('group.edit', [$group])) -> assertStatus(403);
      $this -> actingAs($user) -> put(route('group.update', [$group])) -> assertStatus(403);
      $this -> actingAs($user) -> delete(route('group.destroy', [$group])) -> assertStatus(403);

    }

    public function test_access_from_authenticated_user_within_group()
    {
      $group = factory(\App\Group::class)->create();
      $user = factory(User::class)->create();
      $user->groups()->attach($group);

      $this -> actingAs($user) -> get(route('group.create')) -> assertStatus(200);
      $this -> actingAs($user) -> followingRedirects() -> post(route('group.store')) -> assertStatus(200);
      $this -> actingAs($user) -> get(route('group.show', [$group])) -> assertStatus(200);
      $this -> actingAs($user) -> get(route('group.edit', [$group])) -> assertStatus(403);
      $this -> actingAs($user) -> followingRedirects() -> put(route('group.update', [$group])) -> assertStatus(403);
      $this -> actingAs($user) -> followingRedirects() -> delete(route('group.destroy', [$group])) -> assertStatus(403);
    }

    public function test_access_from_authenticated_admin_user_within_group()
    {
      $group = factory(\App\Group::class)->create();
      $user = factory(User::class)->create();
      $user->groups()->attach($group, ['is_admin' => true]);

      $this -> actingAs($user) -> get(route('group.create')) -> assertStatus(200);
      $this -> actingAs($user) -> followingRedirects() -> post(route('group.store')) -> assertStatus(200);
      $this -> actingAs($user) -> get(route('group.show', [$group])) -> assertStatus(200);
      $this -> actingAs($user) -> get(route('group.edit', [$group])) -> assertStatus(200);
      $this -> actingAs($user) -> followingRedirects() -> put(route('group.update', [$group])) -> assertStatus(200);
      $this -> actingAs($user) -> followingRedirects() -> delete(route('group.destroy', [$group])) -> assertStatus(200);
    }
}
